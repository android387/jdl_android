package com.jdl.distribution.ui.fragment.programados.model;

import androidx.room.Ignore;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.mvp.model.Moneda;

import org.parceler.Parcel;

import java.util.List;

@Parcel(Parcel.Serialization.BEAN)
public class InitVisitResponse implements IStringNotNull<InitVisitResponse>, StatusConverter {

    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("idvis")
    private String idvis;
//    @Expose
//    @SerializedName("idcli")
//    private String idcli;
    @Ignore
    @Expose
    @SerializedName("moneda")
    private Moneda moneda;
    @Ignore
    @Expose
    @SerializedName("cliente")
    private Cliente cliente;
    @Expose
    @SerializedName("motCierre")
    private List<MotivoCierre> listCierre;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public int getStatus() {
        return status;
    }

    public String getIdvis() {
        return idvis;
    }

    public List<MotivoCierre> getListCierre() {
        return listCierre;
    }

    @Override
    public InitVisitResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class MotivoCierre implements IStringNotNull<MotivoCierre>{

        @StringNotNull
        @Expose
        @SerializedName("mot_id")
        private String mot_id;
        @StringNotNull
        @Expose
        @SerializedName("mot_descripcion")
        private String mot_descripcion;

        public String getMot_id() {
            return mot_id;
        }

        @Override
        public String toString() {
            return "MotivoCierre{" +
                    "mot_id='" + mot_id + '\'' +
                    ", mot_descripcion='" + mot_descripcion + '\'' +
                    '}';
        }

        public String getMot_descripcion() {
            return mot_descripcion;
        }

        @Override
        public MotivoCierre getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Override
    public String toString() {
        return "InitVisitResponse{" +
                "msg='" + msg + '\'' +
                "idvisita='" + idvis + '\'' +
                ", moneda=" + moneda +
                ", cliente=" + cliente +
                ", status=" + status +
                '}';
    }
}
