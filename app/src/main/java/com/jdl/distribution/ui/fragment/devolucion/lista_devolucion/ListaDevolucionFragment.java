package com.jdl.distribution.ui.fragment.devolucion.lista_devolucion;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.ui.fragment.devolucion.devolucion_parcial.DevolucionParcialFragment;
import com.jdl.distribution.ui.fragment.devolucion.devolucion_parcial.DevolucionParcialFragmentBuilder;
import com.jdl.distribution.ui.fragment.devolucion.devolucion_parcial.model.EditarListaDevolucionResponse;
import com.jdl.distribution.ui.fragment.devolucion.dialog.DialogDevolucionTotal;
import com.jdl.distribution.ui.fragment.devolucion.lista_devolucion.adapter.ListaDevolucionAdapter;
import com.jdl.distribution.ui.fragment.devolucion.lista_devolucion.model.ListaDevolucionResponse;
import com.jdl.distribution.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@FragmentWithArgs
public class ListaDevolucionFragment extends BaseFragment implements View.OnClickListener,SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rv_devolucion)
    RecyclerView rv_devolucion;
    @BindView(R.id.img_back)
    ImageCardView img_back;
    @BindView(R.id.ll_top)
    LinearLayout ll_top;
    @BindView(R.id.txt_nombre_local)
    TextView txtNombreLocal;
    @Destroy(type = Type.SWIPE_REFRESH)
    @BindView(R.id.sw_devolucion)
    public SwipeRefreshLayout sw_devolucion;
    @BindView(R.id.edt_buscar)
    EditText edt_buscar;
    @BindView(R.id.pb_search_devolucion)
    ProgressBar pb_search_devolucion;

    private DialogFragment dialogDevolucionTotal;

    @Arg(bundler = ParcelerArgsBundler.class)
    Cliente cliente;
    @Arg
    String idVisita;

    private ArrayList<ListaDevolucionResponse.Documento> listDocumento = new ArrayList<>();
    private ListaDevolucionAdapter devolucionAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_devolucion, container, false);
        ButterKnife.bind(this,view);
        FragmentArgs.inject(this);
        init(savedInstanceState);
        return view;
    }

    private void init(Bundle savedInstanceState) {
        initDialog(savedInstanceState);
        setData();
        setAdapter();
        setListener();
    }

    private void initDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null){

            dialogDevolucionTotal = (DialogDevolucionTotal)getChildFragmentManager().findFragmentByTag(DialogDevolucionTotal.class.getSimpleName());

            if (dialogDevolucionTotal == null)
                dialogDevolucionTotal = new DialogDevolucionTotal();

        }else {
            dialogDevolucionTotal = new DialogDevolucionTotal();
        }
    }

    private void setData() {
        txtNombreLocal.setText(cliente.getNombre());
    }

    private void setListener() {
        img_back.setOnClickListener(this);
        sw_devolucion.setOnRefreshListener(this);
    }

    private void setAdapter() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(),1);
        rv_devolucion.setLayoutManager(layoutManager);
        rv_devolucion.setItemAnimator(new DefaultItemAnimator());

        String id [] = {"1","2","3","4","5","6","7"};
        String total [] = {"S/550.00","S/1200.00","S/350.00","S/420.00","S/850.00","S/400.00","S/250.00"};
        String nombre_documento [] = {"F001-0000001","F001-0000002","F001-0000003","F001-0000004","F001-0000005","F001-0000006","F001-0000007"};
        String fecha [] = {"18/10/2019","18/10/2019","16/10/2019","16/10/2019","15/10/2019","15/10/2019","14/10/2019"};

        listDocumento.clear();

        for (int i = 0; i < total.length; i++){
            ListaDevolucionResponse.Documento documento = new ListaDevolucionResponse.Documento();
            documento.setIdDocumento(id[i]);
            documento.setTotal(total[i]);
            documento.setNombre(nombre_documento[i]);
            documento.setFecha(fecha[i]);
            listDocumento.add(documento);
        }

        devolucionAdapter = new ListaDevolucionAdapter(getContext(), listDocumento, new ListaDevolucionAdapter.DevolucionListener() {
            @Override
            public void onItemClickListenerParcial(ListaDevolucionResponse.Documento documento) {

                List<EditarListaDevolucionResponse.Detalle> list = new ArrayList<>();
                list.clear();

                String id [] = {"1","2","3","4","5","6","7","8","9"};
                String nombre [] = {"Coca-Cola","Arroz Costeño","Agua Mineral","Agua San Mateo","Inka-Kola","Coca-Cola","Arroz Costeño","Agua Mineral","Agua San Mateo"};
                String cantidad [] = {"10","25","15","5","30","10","25","15","5"};
                String codigo [] = {"152653","523652","245874","256302","120215","152653","523652","245874","256302"};

                for (int i = 0; i < nombre.length; i++){
                    EditarListaDevolucionResponse.Detalle detalle = new EditarListaDevolucionResponse.Detalle();
                    detalle.setIdPro(id[i]);
                    detalle.setNombre(nombre[i]);
                    detalle.setCantidad(cantidad[i]);
                    detalle.setCodigo(codigo[i]);
                    list.add(detalle);
                }

                Fragment signupFragment = new DevolucionParcialFragmentBuilder(cliente, documento,"",list).build();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.addToBackStack(null);
                transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
                transaction.add(R.id.main_content, signupFragment, DevolucionParcialFragment.class.getSimpleName());
                transaction.commit();

            }

            @Override
            public void onItemClickListenerTotal(String codigo) {

//                Bundle bundle = new Bundle();
//                bundle.putString(DialogDevolucionTotal.IDCLIENTE,cliente.getIdcli());
//                bundle.putString(DialogDevolucionTotal.IDVISITA,idVisita);
//                bundle.putString(DialogDevolucionTotal.IDDOCUMENTO,codigo);
//                dialogDevolucionTotal.setArguments(bundle);
                dialogDevolucionTotal.showNow(getChildFragmentManager(),DialogDevolucionTotal.class.getSimpleName());

            }
        });
        rv_devolucion.setAdapter(devolucionAdapter);
        sw_devolucion.setRefreshing(false);
    }


    @Override
    public void onClick(View v) {
        if (getActivity()==null)
            return;

        switch (v.getId()){

            case R.id.img_back:
                getActivity().onBackPressed();
                break;

        }
    }

    @Override
    public void onRefresh() {
        if (Constants.verifyInternetAndMqtt(getContext())){
            sw_devolucion.setRefreshing(false);
            return;
        }
        setAdapter();
//        loadData(search);
    }
}
