package com.jdl.distribution.ui.fragment.programados.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.developer.appsupport.utils.Constants;
import com.developer.appsupport.utils.MyOutlineProvider;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgramadosAdapter extends RecyclerView.Adapter<ProgramadosAdapter.ViewHolder> {

    Context context;
    ArrayList<Cliente> listProgramados;
    ProgramadosListener listener;

    public ProgramadosAdapter(Context context, ArrayList<Cliente> listProgramados, ProgramadosListener listener) {
        this.context = context;
        this.listProgramados = listProgramados;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_programados,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(listProgramados.get(position));
    }

    public void filterList(ArrayList<Cliente> listFiltroProgramados) {
        listProgramados = listFiltroProgramados;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listProgramados == null ? 0 : listProgramados.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<Cliente>,View.OnClickListener {
        @BindView(R.id.card_programados)
        CardView card_programados;
        @BindView(R.id.txt_programados_item)
        TextView txt_programados_item;
        @BindView(R.id.txt_cliente_item)
        TextView txt_cliente_item;
        @BindView(R.id.txt_distancia_programados_item)
        TextView txt_distancia_programados_item;
        @BindView(R.id.txt_direccion_programados_item)
        TextView txt_direccion_programados_item;
        @BindView(R.id.cv_init_visit)
        CardView cv_init_visit;
        @BindView(R.id.ll_color_visita)
        LinearLayout ll_color_visita;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                card_programados.setOutlineProvider(new MyOutlineProvider((int) Constants.convertDpiToPx(context, 10), 0.25f));
            }
            cv_init_visit.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.cv_init_visit:
                    if (getAdapterPosition()!=-1){
                        listener.onItemClickListener(listProgramados.get(getAdapterPosition()));
                    }
                    break;
            }
        }

        @Override
        public void bindTo(@Nullable Cliente programados) {
            if (programados!=null){
                txt_programados_item.setText(programados.getNombre());
                txt_cliente_item.setText(programados.getContacto());
                txt_distancia_programados_item.setText(programados.getDistancia());
                txt_direccion_programados_item.setText(programados.getDireccion());

                if (programados.getColorVisita().equals(String.valueOf(com.jdl.distribution.utils.Constants.VISIT_DEFAULT))){
                    ll_color_visita.setBackgroundColor(context.getResources().getColor(R.color.text_plomo));
                }else if (programados.getColorVisita().equals(String.valueOf(com.jdl.distribution.utils.Constants.VISIT_COMPLETED))){
                    ll_color_visita.setBackgroundColor(context.getResources().getColor(R.color.verde));
                }else if (programados.getColorVisita().equals(String.valueOf(com.jdl.distribution.utils.Constants.VISIT_SIN_VENTA))){
                    ll_color_visita.setBackgroundColor(context.getResources().getColor(R.color.rojo_claro));
                }

            }
        }
    }

    public interface ProgramadosListener {
        void onItemClickListener(Cliente programados);
    }

}
