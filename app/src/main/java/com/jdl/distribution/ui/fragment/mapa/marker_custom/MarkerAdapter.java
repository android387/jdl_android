package com.jdl.distribution.ui.fragment.mapa.marker_custom;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.model.Cliente;

public class MarkerAdapter implements GoogleMap.InfoWindowAdapter {

    private final Context context;
    private setListenerMarker listener;
    private GoogleMap mMap;

    public MarkerAdapter(Context context, GoogleMap mMap, setListenerMarker listener) {
        this.context = context;
        this.listener = listener;
        this.mMap = mMap;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.marker_custom, null);

        TextView local = view.findViewById(R.id.txt_local);
        TextView distancia = view.findViewById(R.id.txt_distancia);
        CardView btnIniciar = view.findViewById(R.id.btn_iniciar);
        Cliente cliente = (Cliente) marker.getTag();
        local.setText(cliente.getNombre());
        distancia.setText(cliente.getDistancia());

        if (cliente.getProg() == 1) {
            mMap.setOnInfoWindowClickListener(marker1 -> listener.onClick(cliente));
        } else {
            mMap.setOnInfoWindowClickListener(null);
            btnIniciar.setVisibility(View.GONE);
        }

        view.setPadding(0, 0, 0, 0);
        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.marker_custom, null);

        TextView local = view.findViewById(R.id.txt_local);
        TextView distancia = view.findViewById(R.id.txt_distancia);
        CardView btnIniciar = view.findViewById(R.id.btn_iniciar);

        Cliente cliente = (Cliente) marker.getTag();
        local.setText(cliente.getNombre());
        distancia.setText(cliente.getDistancia());

        if (cliente.getProg() == 1) {
            mMap.setOnInfoWindowClickListener(marker1 -> listener.onClick(cliente));
        } else {
            mMap.setOnInfoWindowClickListener(null);
            btnIniciar.setVisibility(View.GONE);
        }
        view.setPadding(0, 0, 0, 0);

        return view;
    }

    public interface setListenerMarker {
        void onClick(Cliente cliente);
    }

}
