package com.jdl.distribution.ui.fragment.agregar_cliente.presenter;

import android.content.Context;
import android.util.Log;

import com.developer.appsupport.utils.asynchttp.CustomHttpResponseHandler;
import com.developer.appsupport.utils.asynchttp.JSONHttpResponseHandler;
import com.google.gson.Gson;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.ui.fragment.agregar_cliente.model.AgregarClienteService;
import com.jdl.distribution.ui.fragment.agregar_cliente.model.MigoDocumentoResponse;
import com.jdl.distribution.ui.fragment.agregar_cliente.view.AgregarClienteView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;

import cz.msebera.android.httpclient.Header;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgregarClientePresenter extends MvpBasePresenter<AgregarClienteView> {

    private static final String LOG_TAG = "[" + AgregarClientePresenter.class.getSimpleName() + "]";
    private final Context context;

    public AgregarClientePresenter(Context context) {
        this.context = context;
    }

    public void doAgregarCliente(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        AgregarClienteService service = RetrofitClient.getRetrofitInstance().create(AgregarClienteService.class);
        Call<StatusMsgResponse> call = service.doAgregarCliente(jsonObject.toString());

        call.enqueue(new Callback<StatusMsgResponse>() {
            @Override
            public void onResponse(Call<StatusMsgResponse> call, Response<StatusMsgResponse> response) {
                StatusMsgResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onAgregarClienteFail("clientListResponse==null");
                        } else {
                            view.onAgregarClienteFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onAgregarClienteSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.onAgregarClienteFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<StatusMsgResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onAgregarClienteFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onAgregarClienteFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

    public void doConsultarDocumentoDni(String token,String dni) {

        //Log.d(LOG_TAG, "enviando: " + jsonObject.toString());
        RetrofitClient.retrofit = null;
        RequestBody tokenR = RequestBody.create(MediaType.parse("multipart/form-data"), token);
        RequestBody dniR = RequestBody.create(MediaType.parse("multipart/form-data"), dni);
        AgregarClienteService service = RetrofitClient.getRetrofitInstance(Constants.URL_BASE_MIGO).create(AgregarClienteService.class);
        Call<MigoDocumentoResponse> call = service.doConsultarDocumentoDni(tokenR,dniR);

       call.enqueue(new Callback<MigoDocumentoResponse>() {
            @Override
            public void onResponse(Call<MigoDocumentoResponse> call, Response<MigoDocumentoResponse> response) {
                MigoDocumentoResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onConsultarDocumentoFail("clientListResponse==null");
                        } else {
                            view.onConsultarDocumentoFail(context.getResources().getString(R.string.error));
                        }
                        RetrofitClient.retrofit = null;
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                int status;

                if (clientListResponse.getStatus()){
                    status = 99;
                }else {
                    status = 0;
                }

                switch (status) {

                    case 99:

                        ifViewAttached(view -> {
                            view.onConsultarDocumentoSuccess(clientListResponse);
                        });
                        RetrofitClient.retrofit = null;
                        break;

                    case 0:
                        ifViewAttached(view -> {
                            view.onConsultarDocumentoFail("Ah ocurrido un error");
                        });
                        RetrofitClient.retrofit = null;
                        break;

                }

            }

            @Override
            public void onFailure(Call<MigoDocumentoResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onConsultarDocumentoFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onConsultarDocumentoFail(context.getResources().getString(R.string.error));
                    }
                    RetrofitClient.retrofit = null;
                });
            }
        });

    }

    public void doConsultarDocumentoRuc(String token,String ruc) {

        //Log.d(LOG_TAG, "enviando: " + jsonObject.toString());
        RetrofitClient.retrofit = null;
        RequestBody tokenR = RequestBody.create(MediaType.parse("multipart/form-data"), token);
        RequestBody rucR = RequestBody.create(MediaType.parse("multipart/form-data"), ruc);
        AgregarClienteService service = RetrofitClient.getRetrofitInstance(Constants.URL_BASE_MIGO).create(AgregarClienteService.class);
        Call<MigoDocumentoResponse> call = service.doConsultarDocumentoRuc(tokenR,rucR);

        call.enqueue(new Callback<MigoDocumentoResponse>() {
            @Override
            public void onResponse(Call<MigoDocumentoResponse> call, Response<MigoDocumentoResponse> response) {
                MigoDocumentoResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onConsultarDocumentoFail("clientListResponse==null");
                        } else {
                            view.onConsultarDocumentoFail(context.getResources().getString(R.string.error));
                        }
                        RetrofitClient.retrofit = null;
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                int status;

                if (clientListResponse.getStatus()){
                    status = 99;
                }else {
                    status = 0;
                }

                switch (status) {

                    case 99:

                        ifViewAttached(view -> {
                            view.onConsultarDocumentoSuccess(clientListResponse);
                        });
                        RetrofitClient.retrofit = null;
                        break;

                    case 0:
                        ifViewAttached(view -> {
                            view.onConsultarDocumentoFail("Ah ocurrido un error");
                        });
                        RetrofitClient.retrofit = null;
                        break;

                }

            }

            @Override
            public void onFailure(Call<MigoDocumentoResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onConsultarDocumentoFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onConsultarDocumentoFail(context.getResources().getString(R.string.error));
                    }
                    RetrofitClient.retrofit = null;
                });
            }
        });

    }

    public void doAgregarImagen(JSONObject jsonObject,File file) {

        //Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("img", file.getName(), fileReqBody);
        RequestBody data = RequestBody.create(MediaType.parse("text/plain"), jsonObject.toString());
        AgregarClienteService service = RetrofitClient.getRetrofitInstance().create(AgregarClienteService.class);
        Call<StatusMsgResponse> call = service.doAgregarImagen(data,part);

        call.enqueue(new Callback<StatusMsgResponse>() {
            @Override
            public void onResponse(Call<StatusMsgResponse> call, Response<StatusMsgResponse> response) {
                StatusMsgResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onAgregarImagenFail("clientListResponse==null");
                        } else {
                            view.onAgregarImagenFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onAgregarImagenSuccess(clientListResponse);
                        });
                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.onAgregarImagenFail(clientListResponse.getMsg());
                        });
                        break;

                }

            }

            @Override
            public void onFailure(Call<StatusMsgResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onAgregarImagenFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onAgregarImagenFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }


   /* public void doConsultarDocumentoRuc(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        AgregarClienteService service = RetrofitClient.getRetrofitInstance().create(AgregarClienteService.class);
        Call<MigoDocumentoResponse> call = service.doConsultarDocumento(jsonObject.toString());

        call.enqueue(new Callback<MigoDocumentoResponse>() {
            @Override
            public void onResponse(Call<MigoDocumentoResponse> call, Response<MigoDocumentoResponse> response) {
                MigoDocumentoResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onConsultarDocumentoFail("clientListResponse==null");
                        } else {
                            view.onConsultarDocumentoFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onConsultarDocumentoSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.onConsultarDocumentoFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<MigoDocumentoResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onConsultarDocumentoFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onConsultarDocumentoFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }*/

}
