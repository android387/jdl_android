package com.jdl.distribution.ui.activity.login.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.ui.activity.login.model.LoginResponse;

public interface LoginView extends MvpView {
    void onLoginSuccess(LoginResponse loginResponse);
    void onLoginFail(String message);
}
