package com.jdl.distribution.ui.fragment.incidencias.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.developer.appsupport.mvp.model.Status;
import com.developer.appsupport.utils.asynchttp.CustomHttpResponseHandler;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.ui.fragment.incidencias.model.IncidenciasResponse;
import com.jdl.distribution.ui.fragment.incidencias.model.IncidenciasService;
import com.jdl.distribution.ui.fragment.incidencias.view.IncidenciasView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IncidenciasPresenter extends MvpBasePresenter<IncidenciasView> {
    private static final String LOG_TAG = "[" + IncidenciasPresenter.class.getSimpleName() + "]";
    private final Context context;

    public IncidenciasPresenter(Context context) {
        this.context = context;
    }

    public void doInsertarIncidencia(JSONObject jsonObject, File file) {

        if (Constants.DEBUG) {
//            Toast.makeText(context, "data: "+jsonObject.toString()+"; file: "+file.getAbsolutePath()+" existe: "+file.exists(), Toast.LENGTH_SHORT).show();
        }

        Log.d(LOG_TAG, "data: " + jsonObject.toString() + "; file: " + file.getAbsolutePath() + " existe: " + file.exists());

        ifViewAttached(view -> view.showLoadingDialog(true));

        RequestParams params = new RequestParams();
        params.put("data", jsonObject.toString());
        try {
            params.put("file", file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(65000);
        client.post(Constants.URL_BASE + Constants.WS_AGREGAR_INCIDENCIA, params, new CustomHttpResponseHandler<StatusMsgResponse>(new StatusMsgResponse()) {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable, @NonNull String errorMessage) {
                ifViewAttached(view -> view.showLoadingDialog(false));
                if (Constants.DEBUG) {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, context.getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, @NonNull StatusMsgResponse editPhotoResponse, @NonNull Status status, int reason) {

                Log.d(LOG_TAG, "onSuccess: " + editPhotoResponse.toString());

                editPhotoResponse.getStatusEnum();

                switch (status) {

                    case SUCCESS:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onAgregarIncidenciasSuccess(editPhotoResponse);
                        });

                        break;

                    case FAIl:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onAgregarIncidenciasFail(editPhotoResponse.getMsg());
                        });

                        break;

                }


            }
        });

    }

    public void doListarIncidencia(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));

        IncidenciasService service = RetrofitClient.getRetrofitInstance().create(IncidenciasService.class);
        Call<IncidenciasResponse> call = service.doListar(jsonObject.toString());

        call.enqueue(new Callback<IncidenciasResponse>() {
            @Override
            public void onResponse(Call<IncidenciasResponse> call, Response<IncidenciasResponse> response) {
                IncidenciasResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onListarIncidenciaFail("clientListResponse==null");
                        } else {
                            view.onListarIncidenciaFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListarIncidenciaSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListarIncidenciaFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<IncidenciasResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onListarIncidenciaFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onListarIncidenciaFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

    public void doElimiarIncidencia(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));

        IncidenciasService service = RetrofitClient.getRetrofitInstance().create(IncidenciasService.class);
        Call<StatusMsgResponse> call = service.doEliminarIncidencia(jsonObject.toString());

        call.enqueue(new Callback<StatusMsgResponse>() {
            @Override
            public void onResponse(Call<StatusMsgResponse> call, Response<StatusMsgResponse> response) {
                StatusMsgResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onDeleteIncidenciasFail("clientListResponse==null");
                        } else {
                            view.onDeleteIncidenciasFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onDeleteIncidenciasSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onDeleteIncidenciasFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<StatusMsgResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onDeleteIncidenciasFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onDeleteIncidenciasFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

}
