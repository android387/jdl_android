package com.jdl.distribution.ui.fragment.historial_despacho.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.parceler.Parcel;

import java.util.List;

public class HistorialDespachoResponse implements IStringNotNull<HistorialDespachoResponse>, StatusConverter {

    @Expose
    @SerializedName("listE")
    private List<DetalleDespacho> listP;
    @Expose
    @SerializedName("status")
    private int status;
    @Expose
    @SerializedName("msg")
    private String msg;

    public List<DetalleDespacho> getListP() {
        return listP;
    }

    public void setListP(List<DetalleDespacho> listP) {
        this.listP = listP;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class DetalleDespacho implements IStringNotNull<DetalleDespacho> {

        @Expose
        @SerializedName("idEntrega")
        private String idEntrega;
        @Expose
        @SerializedName("hora")
        private String hora;
        @Expose
        @SerializedName("nom")
        private String nombreCliente;
        @Expose
        @SerializedName("ruc")
        private String ruc;
        @Expose
        @SerializedName("rs")
        private String rs;
        @Expose
        @SerializedName("nomVen")
        private String nomVen;
        @Expose
        @SerializedName("idCom")
        private int idComprobante;
        @Expose
        @SerializedName("tipCom")
        private String tipoComprobante;
        @Expose
        @SerializedName("encargado")
        private String encargado;


        public String getHora() {
            return hora;
        }

        public void setHora(String hora) {
            this.hora = hora;
        }


        public String getNombreCliente() {
            return nombreCliente;
        }

        public void setNombreCliente(String nombreCliente) {
            this.nombreCliente = nombreCliente;
        }

        public String getRuc() {
            return ruc;
        }

        public void setRuc(String ruc) {
            this.ruc = ruc;
        }

        public String getRs() {
            return rs;
        }

        public void setRs(String rs) {
            this.rs = rs;
        }

        public String getNomVen() {
            return nomVen;
        }

        public void setNomVen(String nomVen) {
            this.nomVen = nomVen;
        }

        public String getIdEntrega() {
            return idEntrega;
        }

        public int getIdComprobante() {
            return idComprobante;
        }

        public String getEncargado() {
            return encargado;
        }

        public String getTipoComprobante() {
            return tipoComprobante;
        }

        public void setTipoComprobante(String tipoComprobante) {
            this.tipoComprobante = tipoComprobante;
        }

        @Override
        public String toString() {
            return "DetalleEntrega{" +
                    "idEntrega='" + idEntrega + '\'' +
                    ", hora='" + hora + '\'' +
                    ", nombreCliente='" + nombreCliente + '\'' +
                    ", ruc='" + ruc + '\'' +
                    ", rs='" + rs + '\'' +
                    ", nomVen='" + nomVen + '\'' +
                    ", idComprobante='" + idComprobante + '\'' +
                    ", tipoComprobante='" + tipoComprobante + '\'' +
                    '}';
        }

        @Override
        public DetalleDespacho getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Override
    public HistorialDespachoResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Override
    public String toString() {
        return "HistorialDespachoResponse{" +
                "listP=" + listP +
                ", status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }
}
