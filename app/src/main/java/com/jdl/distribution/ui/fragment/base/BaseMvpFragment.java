package com.jdl.distribution.ui.fragment.base;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.developer.appsupport.ui.fragment.IBaseFragment;
import com.developer.appsupport.ui.listener.MyLifecycleObserver;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public abstract class BaseMvpFragment<V extends MvpView, P extends MvpPresenter<V>> extends MvpFragment<V, P> implements IBaseFragment {
    private MyLifecycleObserver observer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        if (observer!=null)
            observer.onCreate();

        super.onCreate(savedInstanceState);
    }


    @Override
    public void onStart() {

        if (observer!=null)
            observer.onStart();

        super.onStart();
    }


    @Override
    public void onResume() {

        if (observer!=null)
            observer.onResume();

        super.onResume();
    }


    @Override
    public void onPause() {

        if (observer!=null)
            observer.onPause();

        super.onPause();
    }


    @Override
    public void onStop() {

        if (observer!=null)
            observer.onStop();

        super.onStop();
    }


    @Override
    public void onDestroy() {

        if (observer!=null)
            observer.onDestroy();

        super.onDestroy();
    }


    @Override
    public void setLifecycleObserver(MyLifecycleObserver observer){
        this.observer = observer;
    }
}
