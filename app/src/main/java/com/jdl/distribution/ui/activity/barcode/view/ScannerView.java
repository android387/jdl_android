package com.jdl.distribution.ui.activity.barcode.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.model.BuscarProductoResponse;

public interface ScannerView extends MvpView {
    void showLoadingDialog(boolean status);
    void onScannerSuccess(BuscarProductoResponse response);
    void onScannerFail(String mensaje);
}
