package com.jdl.distribution.ui.fragment.clientes.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.ui.fragment.clientes.model.ComboFrecuenciaResponse;
import com.jdl.distribution.ui.fragment.clientes.model.SeleccioneClienteResponse;

public interface ClientesView extends MvpView {
    void showLoadingDialog(boolean status);
    void onListClientsSuccess(ClienteResponse response);
    void onListClientsFail(String response);

    void onSeleccionaClienteSuccess(SeleccioneClienteResponse response);
    void onSeleccionaClienteFail(String mensaje);

    void onListarComboFrecuenciaSuccess(ComboFrecuenciaResponse response);
    void onListarComboFrecuenciaFail(String mensaje);

}
