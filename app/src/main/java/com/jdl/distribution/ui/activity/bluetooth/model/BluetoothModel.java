package com.jdl.distribution.ui.activity.bluetooth.model;

public class BluetoothModel {
    private String nombre = "";
    private String ip6 = "";

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIp6() {
        return ip6;
    }

    public void setIp6(String ip6) {
        this.ip6 = ip6;
    }

    public BluetoothModel(String nombre, String ip6) {
        this.nombre = nombre;
        this.ip6 = ip6;
    }
}
