package com.jdl.distribution.ui.activity.login.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;
import com.jdl.distribution.mvp.model.Moneda;

import java.util.List;

public class LoginResponse implements IStringNotNull<LoginResponse>, StatusConverter {

    @Expose
    @SerializedName("usuario")
    private Usuario usuario;
    @Expose
    @SerializedName("moneda")
    private Moneda moneda;
    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private int status;

    public Usuario getUsuario() {
        return usuario;
    }

    public String getMsg() {
        return msg;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    public static class Usuario implements IStringNotNull<Usuario> {
        @StringNotNull
        @Expose
        @SerializedName("usu_id")
        private String idUsuario;
        @StringNotNull
        @Expose
        @SerializedName("usu_nombre")
        private String nombre;
        @StringNotNull
        @Expose
        @SerializedName("usu_apellido")
        private String apellido;
        @StringNotNull
        @Expose
        @SerializedName("usu_fullName")
        private String fullNombre;
        @StringNotNull
        @Expose
        @SerializedName("usu_documento")
        private String documento;
        @StringNotNull
        @Expose
        @SerializedName("usu_celular")
        private String celular;
        @StringNotNull
        @Expose
        @SerializedName("usu_correo")
        private String correo;
        @StringNotNull
        @Expose
        @SerializedName("usu_direccion")
        private String direccion;
        @StringNotNull
        @Expose
        @SerializedName("usu_perfil_id")
        private String idPerfil;
        @StringNotNull
        @Expose
        @SerializedName("usu_cambiarPrecio")
        private String idCambiarPrecio;

        public String getIdUsuario() {
            return idUsuario;
        }

        public String getNombre() {
            return nombre;
        }

        public String getApellido() {
            return apellido;
        }

        public String getFullNombre() {
            return fullNombre;
        }

        public String getDocumento() {
            return documento;
        }

        public String getCelular() {
            return celular;
        }

        public String getCorreo() {
            return correo;
        }

        public String getDireccion() {
            return direccion;
        }

        public String getIdPerfil() {
            return idPerfil;
        }

        public String getIdCambiarPrecio() {
            return idCambiarPrecio;
        }

        @Override
        public Usuario getWithStringNotNull() {
            return getWithStringNotNull(this);
        }

        @Override
        public String toString() {
            return "Usuario{" +
                    "idUsuario='" + idUsuario + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", apellido='" + apellido + '\'' +
                    ", fullNombre='" + fullNombre + '\'' +
                    ", documento='" + documento + '\'' +
                    ", celular='" + celular + '\'' +
                    ", correo='" + correo + '\'' +
                    ", direccion='" + direccion + '\'' +
                    ", idPerfil='" + idPerfil + '\'' +
                    ", idCambiarPrecio='" + idCambiarPrecio + '\'' +
                    '}';
        }
    }

    @Override
    public LoginResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "usuario=" + usuario +
                ", moneda=" + moneda +
                ", msg='" + msg + '\'' +
                ", status=" + status +
                '}';
    }
}
