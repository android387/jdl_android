package com.jdl.distribution.ui.activity.barcode;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Destroyer;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.utils.JSONGenerator;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.activity.barcode.presenter.ScannerPresenter;
import com.jdl.distribution.ui.activity.barcode.view.ScannerView;
import com.jdl.distribution.ui.activity.base.BaseMvpActivity;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.model.BuscarProductoResponse;
import com.jdl.distribution.utils.Constants;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class SimpleScannerActivity extends BaseMvpActivity<ScannerView, ScannerPresenter>
        implements ZBarScannerView.ResultHandler,ScannerView {

    private static final String TAG = SimpleScannerActivity.class.getSimpleName();

    private ZBarScannerView mScannerView;

    @BindView(R.id.txt_bac_scanner)
    Button txt_bac_scanner;
    @Destroy(type = Type.PROGRESS_BAR)
    @BindView(R.id.pb_scaner)
    public ProgressBar pb_scaner;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.fragment_scanner);
        ButterKnife.bind(this,this);
//        setupToolbar();
        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        mScannerView = new ZBarScannerView(this);
        contentFrame.addView(mScannerView);

        txt_bac_scanner.setOnClickListener(i->{
            finish();
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();
        // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
        // Stop camera on pause
    }

    @NonNull
    @Override
    public ScannerPresenter createPresenter() {
        return new ScannerPresenter(this);
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v(TAG, rawResult.getContents()); // Prints scan results
        Log.v(TAG, rawResult.getBarcodeFormat().getName()); // Prints the scan format (qrcode, pdf417 etc.)

        //Toast.makeText(this, rawResult.getContents(), Toast.LENGTH_SHORT).show();
        //Toast.makeText(this,rawResult.getBarcodeFormat().getName(), Toast.LENGTH_SHORT).show();
        JSONGenerator.getNewInstance(this)
                .requireInternet(true)
                .put("id_usuario", new LoginPref(this).getUid())
                .put("id_perfil", new LoginPref(this).getIdProfile())
                .put("pro_codbar", rawResult.getContents())
                .operate(jsonObject -> {
                    mScannerView.stopCameraPreview();
                    getPresenter().doSearchProduct(jsonObject);
                });


//        Intent intent = new Intent();
//        intent.putExtra("code", rawResult.getContents());
//        setResult(RESULT_OK, intent);
//        finish();
        // If you would like to resume scanning, call this method below:
    }

    @Override
    public void showLoadingDialog(boolean status) {
        if (status) {
            try {
                Destroyer.subscribe(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            pb_scaner.setVisibility(View.VISIBLE);
        } else {
            try {
                Destroyer.unsubscribe(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            pb_scaner.setVisibility(View.GONE);
        }
    }

    @Override
    public void onScannerSuccess(BuscarProductoResponse response) {
        Toast.makeText(this, response.getMsg(), Toast.LENGTH_SHORT).show();
        EventBus.getDefault().post(new Message<>(Constants.OP_NOTIFY_SCANNER, response));
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onScannerFail(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
        mScannerView.resumeCameraPreview(this);
    }
}
