package com.jdl.distribution.ui.fragment.historial_eventos.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.parceler.Parcel;

import java.util.List;

public class HistorialEventoResponse implements IStringNotNull<HistorialEventoResponse>, StatusConverter {

    @Expose
    @SerializedName("listP")
    private List<DetalleEvento> listP;
    @Expose
    @SerializedName("status")
    private int status;
    @Expose
    @SerializedName("msg")
    private String msg;

    public List<DetalleEvento> getListP() {
        return listP;
    }

    public void setListP(List<DetalleEvento> listP) {
        this.listP = listP;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class DetalleEvento implements IStringNotNull<DetalleEvento> {

        @Expose
        @SerializedName("ideve")
        private String idEvento;
        @Expose
        @SerializedName("nomeve")
        private String nombreEvento;
        @Expose
        @SerializedName("hora")
        private String hora;
        @Expose
        @SerializedName("nom")
        private String nombreCliente;
        @Expose
        @SerializedName("ruc")
        private String ruc;
        @Expose
        @SerializedName("rs")
        private String rs;
        @Expose
        @SerializedName("nomVen")
        private String nomVen;
        @Expose
        @SerializedName("idCom")
        private String idComprobante;
        @Expose
        @SerializedName("tipCom")
        private String tipoComprobante;
        @Expose
        @SerializedName("tipoDoc")
        private String tipoDocumento;
        @Expose
        @SerializedName("nameDoc")
        private String nombreDocumento;

        public String getIdEvento() {
            return idEvento;
        }

        public void setIdEvento(String idEvento) {
            this.idEvento = idEvento;
        }

        public String getNombreEvento() {
            return nombreEvento;
        }

        public void setNombreEvento(String nombreEvento) {
            this.nombreEvento = nombreEvento;
        }

        public String getHora() {
            return hora;
        }

        public void setHora(String hora) {
            this.hora = hora;
        }

        public String getTipoDocumento() {
            return tipoDocumento;
        }

        public void setTipoDocumento(String tipoDocumento) {
            this.tipoDocumento = tipoDocumento;
        }

        public String getNombreDocumento() {
            return nombreDocumento;
        }

        public void setNombreDocumento(String nombreDocumento) {
            this.nombreDocumento = nombreDocumento;
        }

        public String getNombreCliente() {
            return nombreCliente;
        }

        public void setNombreCliente(String nombreCliente) {
            this.nombreCliente = nombreCliente;
        }

        public String getRuc() {
            return ruc;
        }

        public void setRuc(String ruc) {
            this.ruc = ruc;
        }

        public String getRs() {
            return rs;
        }

        public void setRs(String rs) {
            this.rs = rs;
        }

        public String getNomVen() {
            return nomVen;
        }

        public void setNomVen(String nomVen) {
            this.nomVen = nomVen;
        }

        public String getIdComprobante() {
            return idComprobante;
        }

        public void setIdComprobante(String idComprobante) {
            this.idComprobante = idComprobante;
        }

        public String getTipoComprobante() {
            return tipoComprobante;
        }

        public void setTipoComprobante(String tipoComprobante) {
            this.tipoComprobante = tipoComprobante;
        }

        @Override
        public String toString() {
            return "DetalleEvento{" +
                    "idEvento='" + idEvento + '\'' +
                    ", nombreEvento='" + nombreEvento + '\'' +
                    ", hora='" + hora + '\'' +
                    ", nombreCliente='" + nombreCliente + '\'' +
                    ", ruc='" + ruc + '\'' +
                    ", rs='" + rs + '\'' +
                    ", nomVen='" + nomVen + '\'' +
                    ", idComprobante='" + idComprobante + '\'' +
                    ", tipoComprobante='" + tipoComprobante + '\'' +
                    '}';
        }

        @Override
        public DetalleEvento getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Override
    public HistorialEventoResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Override
    public String toString() {
        return "HistorialEventoResponse{" +
                "listP=" + listP +
                ", status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }
}
