package com.jdl.distribution.ui.fragment.productos.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.productos.model.ProductoResponse;
import com.jdl.distribution.ui.fragment.productos.model.ProductoService;
import com.jdl.distribution.ui.fragment.productos.view.ProductosView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductoPresenter extends MvpBasePresenter<ProductosView> {
    private static final String LOG_TAG = ProductoPresenter.class.getSimpleName();
    private final Context context;

    public ProductoPresenter(Context context) {
        this.context = context;
    }

    public void doListProgramados(JSONObject jsonObject) {
        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));

        ProductoService service = RetrofitClient.getRetrofitInstance().create(ProductoService.class);
        Call<ProductoResponse> call = service.doProductosListar(jsonObject.toString());
        call.enqueue(new Callback<ProductoResponse>() {
            @Override
            public void onResponse(Call<ProductoResponse> call, Response<ProductoResponse> response) {
                ProductoResponse programadosResponse = response.body();

                if (programadosResponse == null) {
                    Log.d(LOG_TAG, "programadosResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onListarProductoFail("programadosResponse==null");
                        } else {
                            view.onListarProductoFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + programadosResponse.toString());

                switch (programadosResponse.getStatusEnum()) {
                    case SUCCESS:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListarProductoSuccess(programadosResponse);
                        });
                        break;
                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onListarProductoFail(programadosResponse.getMsg());
                        });

                        break;
                }

            }

            @Override
            public void onFailure(Call<ProductoResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onListarProductoFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.showLoadingDialog(false);
                        view.onListarProductoFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }

}
