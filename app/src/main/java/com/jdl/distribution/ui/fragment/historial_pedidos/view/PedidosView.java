package com.jdl.distribution.ui.fragment.historial_pedidos.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.historial_pedidos.model.PedidoHistorialResponse;

public interface PedidosView extends MvpView {
    void showLoadingDialog(boolean status);
    void onPedidosSuccess(PedidoHistorialResponse response);
    void onPedidosFail(String mensaje);

    void onEmitirComprobanteSuccess(EmitirComprobanteResponse response);
    void onEmitirComprobanteSFail(String mensaje);

}
