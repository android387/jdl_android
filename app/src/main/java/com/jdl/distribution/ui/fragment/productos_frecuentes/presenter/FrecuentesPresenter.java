package com.jdl.distribution.ui.fragment.productos_frecuentes.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.productos_frecuentes.model.FrecuentesService;
import com.jdl.distribution.ui.fragment.productos_frecuentes.model.RegistrarPedidoResponse;
import com.jdl.distribution.ui.fragment.productos_frecuentes.view.FrecuentesView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrecuentesPresenter extends MvpBasePresenter<FrecuentesView> {

    private static final String LOG_TAG = FrecuentesPresenter.class.getSimpleName();
    private final Context context;

    public FrecuentesPresenter(Context context) {
        this.context = context;
    }

    public void doRegistrarPedido(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        FrecuentesService service = RetrofitClient.getRetrofitInstance().create(FrecuentesService.class);
        Call<RegistrarPedidoResponse> call = service.doRegistrarPedido(jsonObject.toString());
        call.enqueue(new Callback<RegistrarPedidoResponse>() {
            @Override
            public void onResponse(Call<RegistrarPedidoResponse> call, Response<RegistrarPedidoResponse> response) {

                RegistrarPedidoResponse registrarPedidoResponse = response.body();

                if (registrarPedidoResponse == null) {
                    Log.d(LOG_TAG, "registrarPedidoResponse==null");
                    if (Constants.DEBUG) {
                        ifViewAttached(view -> view.onRegistrarPedidoFail("registrarPedidoResponse==null"));
                    } else {
                        ifViewAttached(view -> view.onRegistrarPedidoFail(context.getResources().getString(R.string.error)));
                    }
                    return;
                }

                Log.d(LOG_TAG, registrarPedidoResponse.toString());

                switch (registrarPedidoResponse.getWithStringNotNull().getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onRegistrarPedidoSuccess(registrarPedidoResponse);
                        });

                        break;

                    case FAIL:

                        ifViewAttached(view -> {
                            view.onRegistrarPedidoFail(registrarPedidoResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<RegistrarPedidoResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onRegistrarPedidoFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onRegistrarPedidoFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

    public void doEmitirComprobantePreventaRegister(JSONObject jsonObject){

        Log.d(LOG_TAG," enviando: "+jsonObject.toString());

        FrecuentesService service = RetrofitClient.getRetrofitInstance().create(FrecuentesService.class);
        Call<EmitirComprobanteResponse> call = service.emitirComprobante(jsonObject.toString());
        call.enqueue(new Callback<EmitirComprobanteResponse>() {
            @Override
            public void onResponse(Call<EmitirComprobanteResponse> call, Response<EmitirComprobanteResponse> response) {
                EmitirComprobanteResponse emitirComprobanteResponse = response.body();

                if (emitirComprobanteResponse ==null){
                    Log.d(LOG_TAG,"doEmitirComprobantePreventaRegister==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onEmitirComprobanteSFail("doEmitirComprobantePreventaRegister==null");
                        } else {
                            view.onEmitirComprobanteSFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG,emitirComprobanteResponse.toString());

                switch (emitirComprobanteResponse.getStatusEnum()){

                    case SUCCESS:

                        ifViewAttached(view -> view.onEmitirComprobanteSuccess(emitirComprobanteResponse));

                        break;

                    case FAIL:

                        ifViewAttached(view -> view.onEmitirComprobanteSFail(emitirComprobanteResponse.getMsg()));

                        break;

                }
            }

            @Override
            public void onFailure(Call<EmitirComprobanteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());
                ifViewAttached(view -> {

                    if (Constants.DEBUG) {
                        view.onEmitirComprobanteSFail("onFailure: "+t==null?"":t.toString());
                    } else {
                        view.onEmitirComprobanteSFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }

}
