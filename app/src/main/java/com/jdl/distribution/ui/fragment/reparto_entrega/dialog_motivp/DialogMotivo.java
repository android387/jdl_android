package com.jdl.distribution.ui.fragment.reparto_entrega.dialog_motivp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.developer.appsupport.annotation.onetime.OneTime;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.jdl.distribution.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogMotivo extends BaseDialogFragment implements View.OnClickListener {

    @OneTime
    @BindView(R.id.btn_aceptar)
    ProgressButton btn_aceptar;
    @BindView(R.id.btn_cancelar)
    ProgressButton btn_cancelar;
    @BindView(R.id.txt_nombre_factura)
    TextView txt_nombre_factura;

    private setListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_motivo, container, false);
        ButterKnife.bind(this,view);
        init();
        return view;
    }

    private void init(){
        txt_nombre_factura.setText("");
        setListenerClick();
    }

    private void setListenerClick() {
        btn_cancelar.setOnClickListener(this);
        btn_aceptar.setOnClickListener(this);
        btn_aceptar.setOneTimerSupport(true);


    }

    public void setListenerMotivo(setListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_aceptar:
                listener.onClick(btn_aceptar);
                break;
            case R.id.btn_cancelar:
                dismiss();
                break;
        }
    }

    public interface setListener{
        void onClick(ProgressButton button);
    }

}
