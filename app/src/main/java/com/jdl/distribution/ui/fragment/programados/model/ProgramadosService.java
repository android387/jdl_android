package com.jdl.distribution.ui.fragment.programados.model;

import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ProgramadosService {

    @POST(Constants.WS_PROGRAMADOS)
    Call<ClienteResponse> doProgramadosListar(@Body String data);
}
