package com.jdl.distribution.ui.activity.login.model;

import com.google.gson.JsonObject;
import com.jdl.distribution.utils.Constants;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface LoginService {

    @Headers( "Content-Type: application/json; charset=utf-8")
    @POST(Constants.WS_LOGIN)
    Call<LoginResponse> login(@Body String data);

}
