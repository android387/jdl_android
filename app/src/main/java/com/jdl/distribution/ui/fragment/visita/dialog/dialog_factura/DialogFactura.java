package com.jdl.distribution.ui.fragment.visita.dialog.dialog_factura;


import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.ui.fragment.base.BaseMvpDialogFragment;
import com.jdl.distribution.ui.fragment.visita.dialog.dialog_factura.adapter.FacturaAdapter;
import com.jdl.distribution.ui.fragment.visita.dialog.dialog_factura.presenter.FacturaPresenter;
import com.jdl.distribution.ui.fragment.visita.dialog.dialog_factura.view.FacturaView;
import com.jdl.distribution.ui.fragment.visita.model.EntregaTotalResponse;
import com.jdl.distribution.ui.fragment.visita.model.EntregaValidarResponse;
import com.jdl.distribution.utils.Constants;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogFactura extends BaseMvpDialogFragment<FacturaView,FacturaPresenter> implements FacturaView {

    public static final String CLIENTE = "CLIENTE";
    public static final String RESPONSE = "RESPONSE";
    public static final String VISITA = "VISITA";

    @BindView(R.id.rv_lista_factura)
    RecyclerView rv_lista_factura;

    FacturaAdapter adapter;

    //private Cliente cliente;
    private ArrayList<EntregaValidarResponse.Factura> response;
    //private String visita;
    private LoginPref loginPref;

    private setListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_factura, container, false);
        ButterKnife.bind(this, view);
        init(savedInstanceState);
        return view;
    }

    private void init(Bundle savedInstanceState) {

        loginPref = new LoginPref(getContext());

       /* try {
            cliente = Parcels.unwrap(getArguments().getParcelable(CLIENTE));
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        try {
            assert getArguments() != null;
            response = getArguments().getParcelableArrayList(RESPONSE);
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* try {
            visita = getArguments().getString(VISITA);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        setAdapter();
    }


    private void setAdapter() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        rv_lista_factura.setLayoutManager(layoutManager);
        rv_lista_factura.setItemAnimator(new DefaultItemAnimator());

        adapter = new FacturaAdapter(getContext(), response, factura -> {

           /* JSONGenerator.getNewInstance(false, getActivity())
                    .requireInternet(true)
                    .put("idcli", cliente.getIdcli())
                    .put("idpf", new LoginPref(getContext()).getIdProfile())
                    .put("idfv", new LoginPref(getContext()).getUid())
                    .put("idAsig", factura.getIdAsignacion())
                    .operate(jsonObject -> {

                        if (loginPref.getIdProfile().equals(Constants.PROFILE_REPARTO))
                            presenter.doEntregaTotalRequest(jsonObject);
                    });
*/
           String nombre[]={"Agua San Mateo","Azucar","Pepsi","Inka-Kola"};
           String codigo[]={"758421215","4515481545","452123235","1545124542"};
           String cantidad[]={"100","300","200","100"};

           ArrayList<EntregaTotalResponse.Entrega> list = new ArrayList<>();
           for (int i = 0; i < nombre.length; i++){
               EntregaTotalResponse.Entrega response1 = new EntregaTotalResponse.Entrega();
                response1.setNomPro(nombre[i]);
                response1.setCodPro(codigo[i]);
                response1.setCantidad(cantidad[i]);
               list.add(response1);
           }

            listener.goEntregaView(list);
            dismiss();


        });
        rv_lista_factura.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public FacturaPresenter createPresenter() {
        return new FacturaPresenter(getContext());
    }

    @Override
    public void onEntregaTotalSuccess(EntregaTotalResponse response) {
        if (getActivity() == null)return;
        //listener.goEntregaView(response);
        //dismiss();
    }

    @Override
    public void onEntregaTotalFail(String message) {
        if (getActivity() == null)return;
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void onSetListenerSuccess(setListener listener){
        this.listener = listener;
    }

    public interface setListener{
        void goEntregaView(List<EntregaTotalResponse.Entrega> response);
    }

}
