package com.jdl.distribution.ui.fragment.productos_frecuentes.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.productos_frecuentes.model.RegistrarPedidoResponse;

public interface FrecuentesView extends MvpView {
    void onRegistrarPedidoSuccess(RegistrarPedidoResponse response);
    void onRegistrarPedidoFail(String mensaje);

    void onEmitirComprobanteSuccess(EmitirComprobanteResponse response);
    void onEmitirComprobanteSFail(String mensaje);
}
