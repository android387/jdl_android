package com.jdl.distribution.ui.fragment.incidencias.dialog;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.jdl.distribution.R;

public class DialogIncidencias extends BaseDialogFragment {

    public static String IMAGEN = "IMAGEN";

    @BindView(R.id.img_incidencia)
    ImageView img_incidencia;

    private String imagen;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_incidencias, container, false);
        ButterKnife.bind(this,view);
        init();
        return view;
    }

    private void init(){
        try {
            imagen = getArguments().getString(IMAGEN);
        }catch (Exception e){
         e.printStackTrace();
        }
        byte[] base64Decoded = android.util.Base64.decode(imagen, android.util.Base64.DEFAULT);

        Glide.with(getContext())
                .load(base64Decoded)
                .centerCrop()
                .into(img_incidencia);

    }

}
