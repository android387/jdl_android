package com.jdl.distribution.ui.fragment.visita.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.ui.fragment.clientes.model.ClienteService;
import com.jdl.distribution.ui.fragment.clientes.model.ComboFrecuenciaResponse;
import com.jdl.distribution.ui.fragment.clientes.model.SeleccioneClienteResponse;
import com.jdl.distribution.ui.fragment.visita.model.EntregaTotalResponse;
import com.jdl.distribution.ui.fragment.visita.model.EntregaValidarResponse;
import com.jdl.distribution.ui.fragment.visita.model.EventoResponse;
import com.jdl.distribution.ui.fragment.visita.model.MenuVisitaService;
import com.jdl.distribution.ui.fragment.visita.model.ProductoFrecuenteResponse;
import com.jdl.distribution.ui.fragment.visita.view.MenuVisitaView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuVisitaPresenter extends MvpBasePresenter<MenuVisitaView> {

    private static final String LOG_TAG = MenuVisitaPresenter.class.getSimpleName();
    private final Context context;

    public MenuVisitaPresenter(Context context) {
        this.context = context;
    }

    public void doProductosFrecuentes(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        MenuVisitaService service = RetrofitClient.getRetrofitInstance().create(MenuVisitaService.class);
        Call<ProductoFrecuenteResponse> call = service.doProductosFrecuentes(jsonObject.toString());
        call.enqueue(new Callback<ProductoFrecuenteResponse>() {
            @Override
            public void onResponse(Call<ProductoFrecuenteResponse> call, Response<ProductoFrecuenteResponse> response) {

                ProductoFrecuenteResponse frequentProductListResponse = response.body();

                if (frequentProductListResponse == null) {
                    Log.d(LOG_TAG, "frequentProductListResponse==null");
                    if (Constants.DEBUG) {
                        ifViewAttached(view -> view.onProductosFrecuentesFail("frequentProductListResponse==null"));
                    } else {
                        ifViewAttached(view -> view.onProductosFrecuentesFail(context.getResources().getString(R.string.error)));
                    }
                    return;
                }

                Log.d(LOG_TAG, frequentProductListResponse.toString());

                switch (frequentProductListResponse.getWithStringNotNull().getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onProductosFrecuentesSuccess(frequentProductListResponse);
                        });

                        break;

                    case FAIL:

                        ifViewAttached(view -> {
                            view.onProductosFrecuentesFail(frequentProductListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<ProductoFrecuenteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onProductosFrecuentesFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onProductosFrecuentesFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

    public void doCloseVisitRequest(JSONObject jsonObject) {
        Log.d(LOG_TAG, "doCloseVisitRequest " + jsonObject.toString());
//        ifViewAttached(view -> view.showLoading(true));

        MenuVisitaService service = RetrofitClient.getRetrofitInstance().create(MenuVisitaService.class);
        Call<StatusMsgResponse> call = service.doCerrarVisita(jsonObject.toString());
        call.enqueue(new Callback<StatusMsgResponse>() {
            @Override
            public void onResponse(Call<StatusMsgResponse> call, Response<StatusMsgResponse> response) {

                StatusMsgResponse closeVisitResponse = response.body();

                if (closeVisitResponse == null) {
                    Log.d(LOG_TAG, "closeVisitResponse==null");
                    ifViewAttached(view -> {
                        view.onCloseVisitFail(context.getResources().getString(R.string.error));
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + closeVisitResponse.toString());

                switch (closeVisitResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onCloseVisitSuccess(closeVisitResponse);
//                            view.showLoading(false);
                        });

                        break;

                    case FAIL:

                        ifViewAttached(view -> {
                            view.onCloseVisitFail(closeVisitResponse.getMsg());
//                            view.showLoading(false);
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<StatusMsgResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onCloseVisitFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onCloseVisitFail(context.getResources().getString(R.string.error));
                    }
//                    view.showLoading(false);
                });
            }
        });
    }

    public void doSeleccionaCliente(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());


        ClienteService service = RetrofitClient.getRetrofitInstance().create(ClienteService.class);
        Call<SeleccioneClienteResponse> call = service.doSeleccionaCliente(jsonObject.toString());


        call.enqueue(new Callback<SeleccioneClienteResponse>() {
            @Override
            public void onResponse(Call<SeleccioneClienteResponse> call, Response<SeleccioneClienteResponse> response) {
                SeleccioneClienteResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onSeleccionaClienteFail("clientListResponse==null");
                        } else {
                            view.onSeleccionaClienteFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onSeleccionaClienteSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.onSeleccionaClienteFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<SeleccioneClienteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onSeleccionaClienteFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onSeleccionaClienteFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

    public void doListarFrecuencia(JSONObject jsonObject) {

        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());


        ClienteService service = RetrofitClient.getRetrofitInstance().create(ClienteService.class);
        Call<ComboFrecuenciaResponse> call = service.doListarComboFrecuencia(jsonObject.toString());


        call.enqueue(new Callback<ComboFrecuenciaResponse>() {
            @Override
            public void onResponse(Call<ComboFrecuenciaResponse> call, Response<ComboFrecuenciaResponse> response) {
                ComboFrecuenciaResponse clientListResponse = response.body();

                if (clientListResponse == null) {
                    Log.d(LOG_TAG, "clientListResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onListarComboFrecuenciaFail("clientListResponse==null");
                        } else {
                            view.onListarComboFrecuenciaFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + clientListResponse.toString());

                switch (clientListResponse.getStatusEnum()) {

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onListarComboFrecuenciaSuccess(clientListResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.onListarComboFrecuenciaFail(clientListResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<ComboFrecuenciaResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onListarComboFrecuenciaFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onListarComboFrecuenciaFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

    public void doRepartoValidarEntregaRequest(JSONObject jsonObject){

        Log.d(LOG_TAG,"enviando: "+jsonObject.toString());

        MenuVisitaService service = RetrofitClient.getRetrofitInstance().create(MenuVisitaService.class);
        Call<EntregaValidarResponse> call = service.getEntregaValidar(jsonObject.toString());
        call.enqueue(new Callback<EntregaValidarResponse>() {
            @Override
            public void onResponse(Call<EntregaValidarResponse> call, Response<EntregaValidarResponse> response) {

                EntregaValidarResponse entregaValidarResponse = response.body();

                if (entregaValidarResponse==null){
                    Log.d(LOG_TAG, "frequentProductListResponse==null");
                    if (Constants.DEBUG) {
                        ifViewAttached(view -> view.onEntregaValidarFail("frequentProductListResponse==null"));
                    }else {
                        ifViewAttached(view -> view.onEntregaValidarFail(context.getResources().getString(R.string.error)));
                    }
                    return;
                }

                Log.d(LOG_TAG,entregaValidarResponse.toString());

                switch (entregaValidarResponse.getWithStringNotNull().getStatusEnum()){

                    case SUCCESS:

                        ifViewAttached(view -> {
                            view.onEntregaValidarSuccess(entregaValidarResponse);
                        });

                        break;

                    case FAIL:

                        ifViewAttached(view -> {
                            view.onEntregaValidarFail(entregaValidarResponse.getMsg());
                        });

                        break;

                }

            }

            @Override
            public void onFailure(Call<EntregaValidarResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG){
                        view.onEntregaValidarFail("onFailure: "+t==null?"":t.toString());
                    }else {
                        view.onEntregaValidarFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });

    }

    public void doEntregaTotalRequest(JSONObject jsonObject) {
        Log.d(LOG_TAG, "enviando: "+jsonObject.toString());

        MenuVisitaService service = RetrofitClient.getRetrofitInstance().create(MenuVisitaService.class);
        Call<EntregaTotalResponse> call = service.getEntregaTotal(jsonObject.toString());
        call.enqueue(new Callback<EntregaTotalResponse>() {
            @Override
            public void onResponse(Call<EntregaTotalResponse> call, Response<EntregaTotalResponse> response) {

                EntregaTotalResponse entregaResponse = response.body();

                if (entregaResponse==null){
                    Log.d(LOG_TAG, "doEntregaRequest==null");
                    if (Constants.DEBUG) {
                        ifViewAttached(view -> view.onEntregaTotalFail("doEntregaRequest==null"));
                    }else {
                        ifViewAttached(view -> view.onEntregaTotalFail(context.getResources().getString(R.string.error)));
                    }
                    return;
                }

                Log.d(LOG_TAG, "onResponse: "+entregaResponse.toString());

                switch (entregaResponse.getWithStringNotNull().getStatusEnum()){

                    case SUCCESS:

                        ifViewAttached(view -> view.onEntregaTotalSuccess(entregaResponse));

                        break;

                    case FAIL:

                        ifViewAttached(view -> view.onEntregaTotalFail(entregaResponse.getMsg()));

                        break;

                }

            }

            @Override
            public void onFailure(Call<EntregaTotalResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());
                ifViewAttached(view -> {
                if (Constants.DEBUG){
                    view.onEntregaTotalFail("onFailure: "+t==null?"":t.toString());
                }else {
                    view.onEntregaTotalFail(context.getResources().getString(R.string.error));
                }
                });
            }
        });
    }

}
