package com.jdl.distribution.ui.fragment.visita.dialog.dialog_factura.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.ui.fragment.visita.model.EntregaTotalResponse;

public interface FacturaView extends MvpView {
    void onEntregaTotalSuccess(EntregaTotalResponse response);
    void onEntregaTotalFail(String message);
}
