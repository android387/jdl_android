package com.jdl.distribution.ui.fragment.visita.dialog;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.developer.appsupport.annotation.onetime.OneTime;
import com.developer.appsupport.annotation.onetime.OneTimer;
import com.developer.appsupport.mvp.model.Message;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.AppPref;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.ui.fragment.base.BaseMvpDialogFragment;
import com.jdl.distribution.ui.fragment.programados.model.InitVisitResponse;
import com.jdl.distribution.ui.fragment.visita.VisitaFragment;
import com.jdl.distribution.ui.fragment.visita.dialog.mvp_cerrar_visita.presenter.CerrarVisitaPresenter;
import com.jdl.distribution.ui.fragment.visita.dialog.mvp_cerrar_visita.view.CerrarVisitaView;
import com.jdl.distribution.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.parceler.Parcels;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogCerrarVisita extends BaseMvpDialogFragment<CerrarVisitaView, CerrarVisitaPresenter> implements View.OnClickListener, CerrarVisitaView {

    public static final String CLIENTE_RESPONSE = "CLIENTE_RESPONSE";
    public static final String VISITA_RESPONSE = "VISITA_RESPONSE";

    @BindView(R.id.img_close)
    ImageCardView img_close;
    @OneTime
    @BindView(R.id.btn_confirmar)
    ProgressButton btn_confirmar;
    @BindView(R.id.edt_observaciones)
    EditText edt_observaciones;
    @BindView(R.id.spinner_motivos)
    Spinner spinner_motivos;

    private LoginPref loginPref;
    private AppPref appPref;

    private InitVisitResponse visitResponse;
    private Cliente clienteResponse;

    private List<String> listFailStr;
    private String selectMotivo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_cerrar_visita, container, false);
        ButterKnife.bind(this, view);
        setCancelable(false);
        init();
        return view;
    }

    private void init() {

        loginPref = new LoginPref(getActivity());
        appPref = new AppPref(getActivity());

        try {
            visitResponse = Parcels.unwrap(getArguments().getParcelable(VISITA_RESPONSE));
        } catch (Exception e) {

        }
        try {
            clienteResponse = Parcels.unwrap(getArguments().getParcelable(CLIENTE_RESPONSE));
        } catch (Exception e) {

        }
        setListener();
        setMotivosFail();
    }

    private void setMotivosFail() {
        listFailStr = new ArrayList<>();
        if (visitResponse.getListCierre() != null) {
            listFailStr.clear();
            for (InitVisitResponse.MotivoCierre fail : visitResponse.getListCierre()) {
                listFailStr.add(fail.getMot_descripcion());
            }
            ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, listFailStr);
            spinner_motivos.setAdapter(arrayAdapter);
            spinner_motivos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    selectMotivo = visitResponse.getListCierre().get(i).getMot_id();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }
    }

    private void setListener() {
        img_close.setOnClickListener(this);
        btn_confirmar.setOnClickListener(this);
        btn_confirmar.setOneTimerSupport(true);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_close:
                dismiss();
                break;
            case R.id.btn_confirmar:

                JSONGenerator.getNewInstance(getActivity())
                        .requireInternet(true)
                        .put("id_cli", clienteResponse.getIdcli())
                        .put("id_pf", loginPref.getIdProfile())
                        .put("id_fv", loginPref.getUid())
                        .put("lat", appPref.getLatitude())
                        .put("lon", appPref.getLongitude())
                        .put("mot_id", selectMotivo)
                        .put("mot_obs", edt_observaciones.getText().toString())
                        .put("id_vis", visitResponse.getIdvis())
                        .operate(jsonObject -> {

                            OneTimer.subscribe(this, btn_confirmar);
                            getPresenter().doCloseVisitRequest(jsonObject);

                        });
                break;
        }
    }

    @Override
    public CerrarVisitaPresenter createPresenter() {
        return new CerrarVisitaPresenter(getActivity());
    }

    @Override
    public void onCloseVisitSuccess(StatusMsgResponse response) {
        if (getActivity() == null)
            return;

        VisitaFragment.OPEN = false;

        Toast.makeText(getActivity(), response.getMsg(), Toast.LENGTH_SHORT).show();
        EventBus.getDefault().post(new Message<>(Constants.OP_NOTIFY_LOCALES_CERRAR_VISITA, ""));

        getActivity().onBackPressed();
        OneTimer.unsubscribe(this, btn_confirmar);
    }

    @Override
    public void onCloseVisitFail(String message) {
        if (getActivity() == null)
            return;

        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btn_confirmar);
    }
}
