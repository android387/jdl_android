package com.jdl.distribution.ui.fragment.incidencias.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.ui.fragment.incidencias.model.IncidenciasResponse;

public interface IncidenciasView extends MvpView {
    void showLoadingDialog(boolean status);

    void onAgregarIncidenciasSuccess(StatusMsgResponse response);
    void onAgregarIncidenciasFail(String mensaje);

    void onDeleteIncidenciasSuccess(StatusMsgResponse mensaje);
    void onDeleteIncidenciasFail(String mensaje);

    void onListarIncidenciaSuccess(IncidenciasResponse response);
    void onListarIncidenciaFail(String mensaje);
}
