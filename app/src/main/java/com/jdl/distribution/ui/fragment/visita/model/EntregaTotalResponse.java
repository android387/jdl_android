package com.jdl.distribution.ui.fragment.visita.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.parceler.Parcel;

import java.util.List;

public class EntregaTotalResponse implements IStringNotNull<EntregaTotalResponse>, StatusConverter {

    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("listP")
    private List<Entrega> listP;
    @StringNotNull
    @Expose
    @SerializedName("idAsig")
    private String idAsig;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public List<Entrega> getListP() {
        return listP;
    }

    public String getIdAsig() {
        return idAsig;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "EntregaTotalResponse{" +
                "msg='" + msg + '\'' +
                ", listP=" + listP +
                ", idAsig='" + idAsig + '\'' +
                ", status=" + status +
                '}';
    }

    @Parcel(value = Parcel.Serialization.BEAN)
    public static class Entrega implements IStringNotNull<Entrega> {

        @Expose
        @SerializedName("pro_codigo")
        private String idPro;
        @Expose
        @SerializedName("pro_nombre")
        private String nomPro;
        @Expose
        @SerializedName("pro_codigobar")
        private String codPro;
        @Expose
        @SerializedName("vtad_cantidad")
        private String cantidad;
        @Expose
        @SerializedName("vtad_importe")
        private String importe;
        @Expose
        @SerializedName("vtad_codigo")
        private String idVenta;
        /*@StringNotNull
        private String nuevaCantidad = "";
        @StringNotNull
        private String motivo = "";
        private boolean isCkecked = false;
*/

        public String getImporte() {
            return importe;
        }

        public String getIdVenta() {
            return idVenta;
        }

        public void setNomPro(String nomPro) {
            this.nomPro = nomPro;
        }

        public void setCodPro(String codPro) {
            this.codPro = codPro;
        }

        public void setCantidad(String cantidad) {
            this.cantidad = cantidad;
        }

        public String getIdPro() {
            return idPro;
        }

        public String getNomPro() {
            return nomPro;
        }

        public String getCodPro() {
            return codPro;
        }

        public String getCantidad() {
            return cantidad;
        }
/*
        public String getNuevaCantidad() {
            return nuevaCantidad;
        }

        public String getMotivo() {
            return motivo;
        }

        public boolean isCkecked() {
            return isCkecked;
        }

*/
        @Override
        public String toString() {
            return "Entrega{" +
                    "idPro='" + idPro + '\'' +
                    ", nomPro='" + nomPro + '\'' +
                    ", codPro='" + codPro + '\'' +
                    ", cantidad='" + cantidad + '\'' +
                    ", importe='" + importe + '\'' +
                    ", idVenta='" + idVenta + '\'' +
                    /*", nuevaCantidad='" + nuevaCantidad + '\'' +
                    ", motivo='" + motivo + '\'' +
                    ", isCkecked=" + isCkecked +*/
                    '}';
        }

        @Override
        public Entrega getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Override
    public EntregaTotalResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
