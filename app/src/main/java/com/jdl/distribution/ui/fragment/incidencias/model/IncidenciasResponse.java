package com.jdl.distribution.ui.fragment.incidencias.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcel;

import java.util.List;

public class IncidenciasResponse implements IStringNotNull<IncidenciasResponse>, StatusConverter {

    @Expose
    @SerializedName("listF")
    private List<ImagenIncidencia> listF;
    @Expose
    @SerializedName("status")
    private int status;
    @Expose
    @SerializedName("msg")
    private String msg;

    public List<ImagenIncidencia> getListF() {
        return listF;
    }

    public void setListF(List<ImagenIncidencia> listF) {
        this.listF = listF;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class ImagenIncidencia implements IStringNotNull<ImagenIncidencia> {
        @StringNotNull
        @Expose
        @SerializedName("idFoto")
        private String idFoto;
        @Expose
        @SerializedName("image")
        private String image;


        public String getIdFoto() {
            return idFoto;
        }

        public void setIdFoto(String idFoto) {
            this.idFoto = idFoto;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        @Override
        public ImagenIncidencia getWithStringNotNull() {
            return getWithStringNotNull(this);
        }

        @Override
        public String toString() {
            return "ImagenIncidencia{" +
                    "idFoto='" + idFoto + '\'' +
                    ", image='" + image + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "IncidenciasResponse{" +
                "listF=" + listF +
                ", status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }

    @Override
    public IncidenciasResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

}
