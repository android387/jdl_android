package com.jdl.distribution.ui.fragment.visita.dialog.dialog_factura.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;


import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.visita.model.EntregaValidarResponse;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacturaAdapter extends RecyclerView.Adapter<FacturaAdapter.ViewHolder> {

    Context context;
    ArrayList<EntregaValidarResponse.Factura> listFactura;
    OnListener listener;

    public FacturaAdapter(Context context, ArrayList<EntregaValidarResponse.Factura> listFactura, OnListener listener) {
        this.context = context;
        this.listFactura = listFactura;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_factura, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(listFactura.get(position));
    }

    @Override
    public int getItemCount() {
        return listFactura == null ? 0 : listFactura.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<EntregaValidarResponse.Factura>, View.OnClickListener {
        @BindView(R.id.txt_nombre_factura)
        TextView txt_nombre_factura;
        @BindView(R.id.txt_nombre_cpe)
        TextView txt_nombre_cpe;
        @BindView(R.id.txt_nombre_guia)
        TextView txt_nombre_guia;
        @BindView(R.id.icv_factura)
        LinearLayout icv_factura;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            icv_factura.setOnClickListener(this);
        }

        @Override
        public void bindTo(@Nullable EntregaValidarResponse.Factura factura) {
            if (factura != null) {
              /*  txt_nombre_factura.setText(factura.getNombrePedido());
                if (factura.getNombreCpe().isEmpty()){
                    txt_nombre_cpe.setVisibility(View.GONE);
                }else{
                    txt_nombre_cpe.setVisibility(View.VISIBLE);
                    txt_nombre_cpe.setText(factura.getNombreCpe());
                }

                if (factura.getNombreGuia().isEmpty()){
                    txt_nombre_guia.setVisibility(View.GONE);
                }else{
                    txt_nombre_guia.setVisibility(View.VISIBLE);
                    txt_nombre_guia.setText(factura.getNombreGuia());
                }
*/
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.icv_factura:
                    if (getAdapterPosition() != -1)
                        listener.onClickItem(listFactura.get(getAdapterPosition()));
                    break;
            }
        }
    }

    public interface OnListener {
        void onClickItem(EntregaValidarResponse.Factura factura);
    }

}
