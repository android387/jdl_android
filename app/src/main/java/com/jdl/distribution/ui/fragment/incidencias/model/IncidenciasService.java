package com.jdl.distribution.ui.fragment.incidencias.model;

import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IncidenciasService {
    @POST(Constants.WS_AGREGAR_INCIDENCIA)
    Call<StatusMsgResponse> doAgregarIncidencia(@Body String data);

    @POST(Constants.WS_LISTAR_INCIDENCIA)
    Call<IncidenciasResponse> doListar(@Body String data);

    @POST(Constants.WS_ELIMINAR_INCIDENCIA)
    Call<StatusMsgResponse> doEliminarIncidencia(@Body String data);
}
