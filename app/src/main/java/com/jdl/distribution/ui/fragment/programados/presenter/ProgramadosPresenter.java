package com.jdl.distribution.ui.fragment.programados.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.mvp.service.VisitaService;
import com.jdl.distribution.ui.fragment.programados.model.InitVisitResponse;
import com.jdl.distribution.ui.fragment.programados.model.ProgramadosService;
import com.jdl.distribution.ui.fragment.programados.view.ProgramadosView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProgramadosPresenter extends MvpBasePresenter<ProgramadosView> {

    private static final String LOG_TAG = ProgramadosPresenter.class.getSimpleName();
    private final Context context;

    public ProgramadosPresenter(Context context) {
        this.context = context;
    }

    public void doListProgramados(JSONObject jsonObject) {
        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> view.showLoadingDialog(true));

        ProgramadosService service = RetrofitClient.getRetrofitInstance().create(ProgramadosService.class);
        Call<ClienteResponse> call = service.doProgramadosListar(jsonObject.toString());
        call.enqueue(new Callback<ClienteResponse>() {
            @Override
            public void onResponse(Call<ClienteResponse> call, Response<ClienteResponse> response) {
                ClienteResponse programadosResponse = response.body();

                if (programadosResponse == null) {
                    Log.d(LOG_TAG, "programadosResponse==null");
                    ifViewAttached(view -> view.showLoadingDialog(false));
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onProgramadosListFail("programadosResponse==null", null);
                        } else {
                            view.onProgramadosListFail(context.getResources().getString(R.string.error), null);
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + programadosResponse.toString());

                switch (programadosResponse.getStatusEnum()) {
                    case SUCCESS:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onProgramadosListSuccess(programadosResponse);
                        });
                        break;
                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onProgramadosListFail(programadosResponse.getMsg(), programadosResponse);
                        });

                        break;
                }

            }

            @Override
            public void onFailure(Call<ClienteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.showLoadingDialog(false);
                        view.onProgramadosListFail("onFailure: " + t == null ? "" : t.toString(), null);
                    } else {
                        view.showLoadingDialog(false);
                        view.onProgramadosListFail(context.getResources().getString(R.string.error), null);
                    }
                });
            }
        });
    }

    public void doInitVisit(JSONObject jsonObject) {
        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        ifViewAttached(view -> {
            view.showLoadingDialog(true);
        });

        VisitaService service = RetrofitClient.getRetrofitInstance().create(VisitaService.class);
        Call<InitVisitResponse> call = service.doIniciarVisita(jsonObject.toString());

        call.enqueue(new Callback<InitVisitResponse>() {
            @Override
            public void onResponse(Call<InitVisitResponse> call, Response<InitVisitResponse> response) {

                InitVisitResponse initVisitResponse = response.body();

                if (initVisitResponse == null) {
                    Log.d(LOG_TAG , "InitVisitResponse==null");
                    ifViewAttached(view -> {
                        view.showLoadingDialog(false);
                    });
                    return;
                }
                Log.d(LOG_TAG, initVisitResponse.toString());

                switch (initVisitResponse.getStatusEnum()) {

                    case SUCCESS:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onInitVisitSuccess(initVisitResponse);
                        });

                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.showLoadingDialog(false);
                            view.onInitVisitFail(initVisitResponse.getMsg());
                        });


                        break;

                }

            }

            @Override
            public void onFailure(Call<InitVisitResponse> call, Throwable t) {
                Log.d(LOG_TAG , "onFailure: " + t == null ? "" : t.toString());

                ifViewAttached(view -> {
                    view.onInitVisitFail(context.getResources().getString(R.string.error));
                    view.showLoadingDialog(false);
                });
            }
        });
    }

    public void doRecuperarVisita(JSONObject jsonObject){
        Log.d(LOG_TAG,"enviando: "+jsonObject.toString());

//        ifViewAttached(view -> view.showLoading(true));

        VisitaService service = RetrofitClient.getRetrofitInstance().create(VisitaService.class);

        Call<InitVisitResponse> call = service.doRecuperarVisita(jsonObject.toString());

        call.enqueue(new Callback<InitVisitResponse>() {

            @Override
            public void onResponse(Call<InitVisitResponse> call, Response<InitVisitResponse> response) {

                InitVisitResponse recuperarVisita=response.body();

                Log.d(LOG_TAG, "onResponse: "+recuperarVisita.toString());

                switch (recuperarVisita.getWithStringNotNull().getStatusEnum()){

                    case SUCCESS:
                        ifViewAttached(view -> {
                            view.onRecuperarVisitaSuccess(recuperarVisita);
                        });
                        break;

                    case FAIL:
                        ifViewAttached(view -> {
                            view.onRecuperarVisitaFail(recuperarVisita.getMsg());
                        });
                        break;

                }

            }

            @Override
            public void onFailure(Call<InitVisitResponse> call, Throwable t) {

                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());

                if (Constants.DEBUG){
                    ifViewAttached(view -> {
                        view.onRecuperarVisitaFail(t == null ? "" : t.toString());
                    });
                }else {
                    ifViewAttached(view -> {
                        view.onRecuperarVisitaFail(context.getResources().getString(R.string.error));
                    });
                }

            }

        });
    }

}
