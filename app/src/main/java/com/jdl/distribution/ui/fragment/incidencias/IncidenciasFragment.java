package com.jdl.distribution.ui.fragment.incidencias;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.annotation.destroy.Destroy;
import com.developer.appsupport.annotation.destroy.Destroyer;
import com.developer.appsupport.annotation.destroy.Type;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.dialog.select_photo.SelectPhotoDialog;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.developer.appsupport.utils.files.ImageListener;
import com.developer.appsupport.utils.files.ImagePresenter;
import com.developer.appsupport.utils.files.ImagePresenterImpl;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.AppPref;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.mvp.model.Moneda;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.incidencias.adapter.IncidenciasAdapter;
import com.jdl.distribution.ui.fragment.incidencias.dialog.DialogIncidencias;
import com.jdl.distribution.ui.fragment.incidencias.model.IncidenciasResponse;
import com.jdl.distribution.ui.fragment.incidencias.presenter.IncidenciasPresenter;
import com.jdl.distribution.ui.fragment.incidencias.view.IncidenciasView;
import com.jdl.distribution.ui.fragment.programados.model.InitVisitResponse;
import com.jdl.distribution.utils.Constants;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

@FragmentWithArgs
public class IncidenciasFragment extends BaseMvpFragment<IncidenciasView, IncidenciasPresenter>
        implements View.OnClickListener, ImageListener, SelectPhotoDialog.OnTypeSelectedListener, IncidenciasView {

    @Arg(bundler = ParcelerArgsBundler.class)
    Cliente cliente;
    @Arg(bundler = ParcelerArgsBundler.class)
    Moneda moneda;
    @Arg
    String idVisita;

    @BindView(R.id.rv_galeria_foto)
    RecyclerView rv_galeria_foto;
    @BindView(R.id.cv_add_imagen)
    CardView cv_add_imagen;

    @BindView(R.id.cv_waze)
    CardView cv_waze;
    @BindView(R.id.cv_maps)
    CardView cv_maps;
    @BindView(R.id.img_back)
    ImageCardView img_back;

    @BindView(R.id.txt_nombre_local)
    TextView txtNombreLocal;
    @BindView(R.id.txt_nombre_cliente)
    TextView txtNombreCliente;
    @BindView(R.id.txt_distancia_local)
    TextView txtDistanciaLocal;
    @BindView(R.id.txt_direccion_local)
    TextView txtDireccionLocal;
    @BindView(R.id.txt_rs)
    TextView txt_rs;
    @BindView(R.id.ll_frecuencia)
    LinearLayout ll_frecuencia;
    @BindView(R.id.txt_frecuencia)
    TextView txt_frecuencia;
    @Destroy(type = Type.PROGRESS_BAR)
    @BindView(R.id.pb_incidencia)
    public ProgressBar pb_incidencia;
    @BindView(R.id.txt_horario_local)
    TextView txt_horario_local;

    private DialogFragment dialogIncidencia;
    private LoginPref loginPref;
    private ImagePresenter imagePresenter;
    private SelectPhotoDialog selectPhotoDialog;
    private IncidenciasAdapter adapter;
    private ArrayList<IncidenciasResponse.ImagenIncidencia> listIncidencias = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incidencias, container, false);
        FragmentArgs.inject(this);
        ButterKnife.bind(this, view);
        init(savedInstanceState);
        return view;
    }

    private void init(Bundle savedInstanceState) {
        imagePresenter = new ImagePresenterImpl(getActivity(), this);
        loginPref = new LoginPref(getContext());
        initDialogs(savedInstanceState);
        setData();
        setAdapter();
        setListener();
        loadIncidencias();
    }

    private void setListener() {
        cv_add_imagen.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }

    private void setData() {
        txtNombreLocal.setText(cliente.getNombre());
        txtNombreCliente.setText(cliente.getContacto());
        txtDistanciaLocal.setText(cliente.getDistancia());
        txtDireccionLocal.setText(cliente.getDireccion());
        txt_rs.setText(cliente.getRazonSocial());
        txt_horario_local.setText(
                cliente.getHoraInicio().isEmpty() ? "" : cliente.getHoraInicio().concat(":00")
                        .concat(cliente.getHoraInicio().isEmpty() || cliente.getHoraFin().isEmpty() ? "" : "-")
                        .concat(cliente.getHoraFin().isEmpty() ? "" : cliente.getHoraFin().concat(":00")));
//        txt_frecuencia.setText(cliente.getFrecuencia().isEmpty() ? "" : cliente.getFrecuencia()+"("+cliente.getDias()+")");
        if (loginPref.getIdProfile().equals(Constants.PROFILE_AUTO_VENTA) ||
                loginPref.getIdProfile().equals(Constants.PROFILE_ADMINISTRADOR) ||
                loginPref.getIdProfile().equals(Constants.PROFILE_SUPERVISOR)) {
            ll_frecuencia.setVisibility(View.VISIBLE);
        }

    }

    private void initDialogs(Bundle savedInstanceState) {

        if (savedInstanceState != null) {

            selectPhotoDialog = (SelectPhotoDialog) getChildFragmentManager().findFragmentByTag(SelectPhotoDialog.class.getSimpleName());
            dialogIncidencia = (DialogIncidencias) getChildFragmentManager().findFragmentByTag(DialogIncidencias.class.getSimpleName());

            if (selectPhotoDialog == null) {
                selectPhotoDialog = new SelectPhotoDialog();
                selectPhotoDialog.setOnTypeSelectedListener(this);
            }
            if (dialogIncidencia == null) {
                dialogIncidencia = new DialogIncidencias();
            }

        } else {
            selectPhotoDialog = new SelectPhotoDialog();
            selectPhotoDialog.setOnTypeSelectedListener(this);
            dialogIncidencia = new DialogIncidencias();
        }

    }

    private void setAdapter() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        rv_galeria_foto.setLayoutManager(layoutManager);
        rv_galeria_foto.setItemAnimator(new DefaultItemAnimator());

        adapter = new IncidenciasAdapter(getContext(), listIncidencias, new IncidenciasAdapter.IncidenciaListener() {
            @Override
            public void onItemClickDeleteListener(IncidenciasResponse.ImagenIncidencia listIncidencia) {
                JSONGenerator.getNewInstance(getActivity())
                        .requireInternet(true)
                        .put("id_cliente", cliente.getIdcli())
                        .put("id_perfil", new LoginPref(getContext()).getIdProfile())
                        .put("id_usuario", new LoginPref(getContext()).getUid())
                        .put("id_incidencia", listIncidencia.getIdFoto())
                        .operate(jsonObject -> getPresenter().doElimiarIncidencia(jsonObject));
            }

            @Override
            public void onItemClick(IncidenciasResponse.ImagenIncidencia listIncidencia) {
                if (dialogIncidencia.isResumed())
                    return;

                Bundle bundle = new Bundle();
                bundle.putString(DialogIncidencias.IMAGEN, listIncidencia.getImage());
                dialogIncidencia.setArguments(bundle);
                dialogIncidencia.showNow(getChildFragmentManager(), DialogIncidencias.class.getSimpleName());
            }
        });

        rv_galeria_foto.setAdapter(adapter);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                getActivity().onBackPressed();
                break;
            case R.id.cv_add_imagen:
                selectPhotoDialog.show(getChildFragmentManager(), SelectPhotoDialog.class.getSimpleName());
                break;
        }
    }

    private void loadIncidencias() {

        JSONGenerator.getNewInstance(getActivity())
                .requireInternet(true)
                .put("id_cliente", cliente.getIdcli())
                .put("id_perfil", new LoginPref(getContext()).getIdProfile())
                .put("id_usuario", new LoginPref(getContext()).getUid())
                .operate(jsonObject -> getPresenter().doListarIncidencia(jsonObject));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imagePresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onTypeSelected(SelectPhotoDialog.ImageProvider imageProvider) {
        switch (imageProvider) {

            case GALLERY:
                imagePresenter.doGetImageGallery();
                break;

            case CAMERA:
                imagePresenter.doGetImageCamera();
                break;
        }
    }

    @Override
    public void onImageLoading(boolean loading, String message) {

    }

    @Override
    public void onImageSuccess(File file) {
        JSONGenerator.getNewInstance(getActivity())
                .put("id_perfil", new LoginPref(getContext()).getIdProfile())
                .put("id_fv", new LoginPref(getContext()).getUid())
                .put("id_cliente", cliente.getIdcli())
                .put("id_visita", idVisita)
                .put("desc", "blablabla")
                .operate(jsonObject -> {
                    getPresenter().doInsertarIncidencia(jsonObject, file);
                });
    }

    @Override
    public void onImageViewSuccess(Bitmap bitmap) {

    }

    @Override
    public void onImageFail(String message) {

    }


    @Override
    public IncidenciasPresenter createPresenter() {
        return new IncidenciasPresenter(getContext());
    }

    @Override
    public void showLoadingDialog(boolean status) {
        if (status) {
            try {
                Destroyer.subscribe(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            pb_incidencia.setVisibility(View.VISIBLE);
        } else {
            try {
                Destroyer.unsubscribe(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            pb_incidencia.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAgregarIncidenciasSuccess(StatusMsgResponse response) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
        loadIncidencias();
    }

    @Override
    public void onAgregarIncidenciasFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteIncidenciasSuccess(StatusMsgResponse mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje.getMsg(), Toast.LENGTH_SHORT).show();
        loadIncidencias();
    }

    @Override
    public void onDeleteIncidenciasFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onListarIncidenciaSuccess(IncidenciasResponse response) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
        if (response.getListF() != null) {
            listIncidencias.clear();
            listIncidencias.addAll(response.getListF());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onListarIncidenciaFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}
