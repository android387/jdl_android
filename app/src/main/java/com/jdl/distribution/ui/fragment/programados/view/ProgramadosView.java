package com.jdl.distribution.ui.fragment.programados.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.ui.fragment.programados.model.InitVisitResponse;

public interface ProgramadosView extends MvpView {
    void showLoadingDialog(boolean status);
    void onProgramadosListSuccess(ClienteResponse response);
    void onProgramadosListFail(String msg,ClienteResponse response);

    void onInitVisitSuccess(InitVisitResponse response);
    void onInitVisitFail(String message);

    void onRecuperarVisitaSuccess(InitVisitResponse response);
    void onRecuperarVisitaFail(String mensaje);

}
