package com.jdl.distribution.ui.fragment.visita.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

public class EventoResponse implements IStringNotNull<EventoResponse>, StatusConverter {

    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("evento")
    private String evento;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EventoResponse{" +
                "msg='" + msg + '\'' +
                ", evento='" + evento + '\'' +
                ", status=" + status +
                '}';
    }

    @Override
    public EventoResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
