package com.jdl.distribution.ui.fragment.historial_despacho.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.utils.Constants;
import com.developer.appsupport.utils.MyOutlineProvider;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.historial_despacho.model.HistorialDespachoResponse;
import com.jdl.distribution.utils.adapter.AdapterBinder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DespachoAdapter extends RecyclerView.Adapter<DespachoAdapter.ViewHolder> {

    private Context context;
    private ArrayList<HistorialDespachoResponse.DetalleDespacho> listEvento;
    private EntregaHistorialListener listener;

    public DespachoAdapter(Context context, ArrayList<HistorialDespachoResponse.DetalleDespacho> listEvento,EntregaHistorialListener listener) {
        this.context = context;
        this.listEvento = listEvento;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_despacho,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(listEvento.get(position));
    }

    @Override
    public int getItemCount() {
        return listEvento.size();
    }

    public void filterList(ArrayList<HistorialDespachoResponse.DetalleDespacho> listFiltroPedido) {
        listEvento = listFiltroPedido;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<HistorialDespachoResponse.DetalleDespacho>,View.OnClickListener {
        @BindView(R.id.txt_nombre_historial)
        TextView txt_nombre_evento_historial;
        @BindView(R.id.txt_hora_historial)
        TextView txt_hora_evento_historial;
        @BindView(R.id.txt_encargado)
        TextView txt_encargado;
        @BindView(R.id.txt_ruc_historial)
        TextView txt_ruc_evento_historial;
        @BindView(R.id.txt_rs_historial)
        TextView txt_rs_evento_historial;
        @BindView(R.id.txt_vendedor_historial)
        TextView txt_vendedor_evento_historial;
        @BindView(R.id.cv_despacho_historial)
        CardView cv_evento_historial;
        @BindView(R.id.txt_factura)
        TextView txt_factura;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cv_evento_historial.setOutlineProvider(new MyOutlineProvider((int) Constants.convertDpiToPx(context, 10), 0.25f));
            }
            cv_evento_historial.setOnClickListener(this);
        }

        @Override
        public void bindTo(@Nullable HistorialDespachoResponse.DetalleDespacho detalleDespacho) {
            if (detalleDespacho!=null){
                txt_nombre_evento_historial.setText(detalleDespacho.getNombreCliente());
                txt_hora_evento_historial.setText("");
                txt_encargado.setText(detalleDespacho.getEncargado());
                txt_ruc_evento_historial.setText(detalleDespacho.getRuc());
                txt_rs_evento_historial.setText(detalleDespacho.getRs());
                txt_vendedor_evento_historial.setText(detalleDespacho.getNomVen());
                if (detalleDespacho.getTipoComprobante().equals(com.jdl.distribution.utils.Constants.FACTURA)) {
                    txt_factura.setText("FACTURA");
                }else{
                    txt_factura.setText("BOLETA");
                }
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.cv_despacho_historial:
                    if (getAdapterPosition()!=-1){
                        listener.onItemClickListener(listEvento.get(getAdapterPosition()));
                    }
                    break;
            }
        }
    }
    public interface EntregaHistorialListener {
        void onItemClickListener(HistorialDespachoResponse.DetalleDespacho detallePedido);
    }
}
