package com.jdl.distribution.ui.fragment.productos.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.parceler.Parcel;

import java.util.List;

public class ProductoResponse implements IStringNotNull<ProductoResponse>, StatusConverter {

    @Expose
    @SerializedName("listP")
    private List<Productos> listP;
    @Expose
    @SerializedName("status")
    private int status;
    @Expose
    @SerializedName("msg")
    private String msg;

    public List<Productos> getListP() {
        return listP;
    }

    public void setListP(List<Productos> listP) {
        this.listP = listP;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "ProductsResponse{" +
                "listP=" + listP +
                ", status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Productos implements IStringNotNull<Productos> {

        @Expose
        @SerializedName("idPro")
        private String idPro;
        @Expose
        @SerializedName("nom")
        private String nom;
        @Expose
        @SerializedName("cost")
        private String cost;
        @Expose
        @SerializedName("desP")
        private String desP;
        @Expose
        @SerializedName("stock")
        private String stock;
        @Expose
        @SerializedName("stockT")
        private String stockT;
        @Expose
        @SerializedName("img")
        private String imagen;

        public String getIdPro() {
            return idPro;
        }

        public void setIdPro(String idPro) {
            this.idPro = idPro;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getDesP() {
            return desP;
        }

        public void setDesP(String desP) {
            this.desP = desP;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getStockT() {
            return stockT;
        }

        public void setStockT(String stockT) {
            this.stockT = stockT;
        }

        public String getImagen() {
            return imagen;
        }

        public void setImagen(String imagen) {
            this.imagen = imagen;
        }

        @Override
        public String toString() {
            return "Productos{" +
                    "idPro='" + idPro + '\'' +
                    ", nom='" + nom + '\'' +
                    ", cost='" + cost + '\'' +
                    ", desP='" + desP + '\'' +
                    ", stock='" + stock + '\'' +
                    ", stockT='" + stockT + '\'' +
                    ", imagen='" + imagen + '\'' +
                    '}';
        }

        @Override
        public Productos getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Override
    public ProductoResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
