package com.jdl.distribution.ui.dialog_fragment.dialog_fragment.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.ui.dialog_fragment.dialog_fragment.model.BuscarProductoResponse;

public interface SearchProductView extends MvpView {
    void showLoadingDialog(boolean status);
    void onSearchProductSuccess(BuscarProductoResponse response);
    void onSearchProductFail(String message);

//    void onSearchProductStockSuccess(SearchProductResponse response);
//    void onSearchProductStockFail(String message);

}
