package com.jdl.distribution.ui.fragment.devolucion.lista_devolucion.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.parceler.Parcel;

import java.util.List;

public class ListaDevolucionResponse implements IStringNotNull<ListaDevolucionResponse>, StatusConverter {

    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("listD")
    private List<Documento> listD;
    @Expose
    @SerializedName("status")
    private int status;

    public String getMsg() {
        return msg;
    }

    public List<Documento> getListD() {
        return listD;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Documento implements IStringNotNull<Documento> {
        @Expose
        @SerializedName("idDoc")
        private String idDocumento;
        @Expose
        @SerializedName("nom")
        private String nombre;
        @Expose
        @SerializedName("fecha")
        private String fecha;
        @Expose
        @SerializedName("total")
        private String total;

        public String getIdDocumento() {
            return idDocumento;
        }

        public void setIdDocumento(String idDocumento) {
            this.idDocumento = idDocumento;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getFecha() {
            return fecha;
        }

        public void setFecha(String fecha) {
            this.fecha = fecha;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        @Override
        public String toString() {
            return "Documento{" +
                    "idDocumento='" + idDocumento + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", fecha='" + fecha + '\'' +
                    ", total='" + total + '\'' +
                    '}';
        }

        @Override
        public Documento getWithStringNotNull() {
            return getWithStringNotNull(this);
        }

    }

    @Override
    public ListaDevolucionResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }


    @Override
    public String toString() {
        return "ListaDevolucionResponse{" +
                "msg='" + msg + '\'' +
                ", listD=" + listD +
                ", status=" + status +
                '}';
    }
}
