package com.jdl.distribution.ui.fragment.reparto_entrega.presenter;

import android.content.Context;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.jdl.distribution.R;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.productos_frecuentes.model.FrecuentesService;
import com.jdl.distribution.ui.fragment.reparto_entrega.model.EntregaResponse;
import com.jdl.distribution.ui.fragment.reparto_entrega.model.EntregaService;
import com.jdl.distribution.ui.fragment.reparto_entrega.view.EntregaView;
import com.jdl.distribution.utils.Constants;
import com.jdl.distribution.utils.retrofit.RetrofitClient;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EntregaPresenter extends MvpBasePresenter<EntregaView> {

    private static final String LOG_TAG = EntregaPresenter.class.getSimpleName();
    private final Context context;

    public EntregaPresenter(Context context) {
        this.context = context;
    }

    public void doEntrega(JSONObject jsonObject) {
        Log.d(LOG_TAG, "enviando: " + jsonObject.toString());

        EntregaService service = RetrofitClient.getRetrofitInstance().create(EntregaService.class);
        Call<EntregaResponse> call = service.doEntrega(jsonObject.toString());
        call.enqueue(new Callback<EntregaResponse>() {
            @Override
            public void onResponse(Call<EntregaResponse> call, Response<EntregaResponse> response) {
                EntregaResponse programadosResponse = response.body();

                if (programadosResponse == null) {
                    Log.d(LOG_TAG, "programadosResponse==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onEntregarFail("programadosResponse==null");
                        } else {
                            view.onEntregarFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG, "onResponse: " + programadosResponse.toString());

                switch (programadosResponse.getStatusEnum()) {
                    case SUCCESS:
                        ifViewAttached(view -> {
                            view.onEntregarSuccess(programadosResponse);
                        });
                        break;
                    case FAIL:
                        ifViewAttached(view -> {
                            view.onEntregarFail(programadosResponse.getMsg());
                        });

                        break;
                }

            }

            @Override
            public void onFailure(Call<EntregaResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: " + t == null ? "" : t.toString());
                ifViewAttached(view -> {
                    if (Constants.DEBUG) {
                        view.onEntregarFail("onFailure: " + t == null ? "" : t.toString());
                    } else {
                        view.onEntregarFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }

    public void doEmitirComprobantePreventaRegister(JSONObject jsonObject){

        Log.d(LOG_TAG," enviando: "+jsonObject.toString());

        FrecuentesService service = RetrofitClient.getRetrofitInstance().create(FrecuentesService.class);
        Call<EmitirComprobanteResponse> call = service.emitirComprobante(jsonObject.toString());
        call.enqueue(new Callback<EmitirComprobanteResponse>() {
            @Override
            public void onResponse(Call<EmitirComprobanteResponse> call, Response<EmitirComprobanteResponse> response) {
                EmitirComprobanteResponse emitirComprobanteResponse = response.body();

                if (emitirComprobanteResponse ==null){
                    Log.d(LOG_TAG,"doEmitirComprobantePreventaRegister==null");
                    ifViewAttached(view -> {
                        if (Constants.DEBUG) {
                            view.onEmitirComprobanteSFail("doEmitirComprobantePreventaRegister==null");
                        } else {
                            view.onEmitirComprobanteSFail(context.getResources().getString(R.string.error));
                        }
                    });
                    return;
                }

                Log.d(LOG_TAG,emitirComprobanteResponse.toString());

                switch (emitirComprobanteResponse.getStatusEnum()){

                    case SUCCESS:

                        ifViewAttached(view -> view.onEmitirComprobanteSuccess(emitirComprobanteResponse));

                        break;

                    case FAIL:

                        ifViewAttached(view -> view.onEmitirComprobanteSFail(emitirComprobanteResponse.getMsg()));

                        break;

                }
            }

            @Override
            public void onFailure(Call<EmitirComprobanteResponse> call, Throwable t) {
                Log.d(LOG_TAG, "onFailure: "+t==null?"":t.toString());
                ifViewAttached(view -> {

                    if (Constants.DEBUG) {
                        view.onEmitirComprobanteSFail("onFailure: "+t==null?"":t.toString());
                    } else {
                        view.onEmitirComprobanteSFail(context.getResources().getString(R.string.error));
                    }
                });
            }
        });
    }
}
