package com.jdl.distribution.ui.fragment.productos_frecuentes.dialog;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.jdl.distribution.R;
import com.savvi.rangedatepicker.CalendarPickerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogCalendario extends BaseDialogFragment implements View.OnClickListener{

    @BindView(R.id.calendar_view)
    CalendarPickerView calendar;
    @BindView(R.id.ll_cancelar)
    LinearLayout ll_cancelar;
    @BindView(R.id.ll_aceptar)
    LinearLayout ll_aceptar;

    private OnFechaListener listener;

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat formatterShort  = new SimpleDateFormat("dd-MM");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_calendario, container, false);
        setCancelable(false);

        ButterKnife.bind(this,view);

        init();

        return view;
    }

    private void setListener(){
        ll_aceptar.setOnClickListener(this);
        ll_cancelar.setOnClickListener(this);
    }

    private void init(){
        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 10);
        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, - 10);

        calendar.init(new Date(), nextYear.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.SINGLE)
                .withSelectedDate(new Date());

        calendar.scrollToDate(new Date());

        setListener();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_cancelar:
                dismiss();
                break;
            case R.id.ll_aceptar:
                listener.onSelectFecha(
                        formatter.format(calendar.getSelectedDate()),
                        formatterShort.format(calendar.getSelectedDate()));
                dismiss();
                break;
        }
    }

    public void setOnSuccessListenerFecha(OnFechaListener listener){
        this.listener = listener;
    }

    public interface OnFechaListener{
        void onSelectFecha(String fecha, String fechaShort);
    }

}
