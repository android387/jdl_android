package com.jdl.distribution.ui.fragment.emitir_comprobante;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.fragment.emitir_comprobante.adapter.EmitirAdapter;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

@FragmentWithArgs
public class EmitirComprobanteFragment extends BaseFragment {

    public static boolean OPEN = true;
    @Arg(bundler = ParcelerArgsBundler.class)
    EmitirComprobanteResponse.Comprobante comprobanteResponse;

    @BindView(R.id.rv_pedido_comprobante)
    RecyclerView rv_pedido_comprobante;
    @BindView(R.id.txt_nombre_local)
    TextView txtNombreLocal;
    @BindView(R.id.txt_tipo_comprobante)
    TextView txt_tipo_comprobante;
    @BindView(R.id.txt_cod_comprobante)
    TextView txt_cod_comprobante;
    @BindView(R.id.txt_nombre_cliente)
    TextView txt_nombre_cliente;
    @BindView(R.id.txt_ruc_cliente)
    TextView txt_ruc_cliente;
    @BindView(R.id.txt_nombre_comprobante)
    TextView txt_nombre_comprobante;
    @BindView(R.id.txt_total)
    TextView txt_total;
    @BindView(R.id.txt_igv)
    TextView txt_igv;
    @BindView(R.id.ll_igv)
    LinearLayout ll_igv;
    @BindView(R.id.txt_fecha_emision_cliente)
    TextView txt_fecha_emision_cliente;
    @BindView(R.id.txt_moneda_cliente)
    TextView txt_moneda_cliente;
    @BindView(R.id.txt_local_cliente)
    TextView txt_local_cliente;
    @BindView(R.id.txt_direccion_cliente)
    TextView txt_direccion_cliente;
    @BindView(R.id.txt_fecha_entrega_cliente)
    TextView txt_fecha_entrega_cliente;
    @BindView(R.id.txt_vendedor)
    TextView txt_vendedor;
    @BindView(R.id.txt_tipo_pago)
    TextView txt_tipo_pago;
    @BindView(R.id.ll_fecha_entrega)
    LinearLayout ll_fecha_entrega;
    @BindView(R.id.ll_ope_grab)
    LinearLayout ll_ope_grab;
    @BindView(R.id.txt_op_grab)
    TextView txt_op_grab;

    @BindView(R.id.txt_domicilio_cliente)
    TextView txt_domicilio_cliente;
    @BindView(R.id.txt_medio_pago)
    TextView txt_medio_pago;

    @BindView(R.id.btn_anular_comprobante)
    Button btn_anular_comprobante;
    @BindView(R.id.btn_editar_pedido)
    Button btn_editar_pedido;
    @BindView(R.id.btn_emitir_comprobante)
    Button btn_emitir_comprobante;
    @BindView(R.id.ll_bottom)
    LinearLayout ll_bottom;
    @BindView(R.id.txt_fv_nombre)
    TextView txt_fv_nombre;

    private EmitirAdapter emitirAdapter;
    private LoginPref loginPref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_emitir_comprobante, container, false);
        ButterKnife.bind(this, view);
        FragmentArgs.inject(this);
        init();
        return view;
    }

    private void init() {
        if (getActivity() == null)
            return;
        if (comprobanteResponse == null) {
            return;
        }
        loginPref = new LoginPref(getContext());
        congufgUI();
        setData(comprobanteResponse);
    }

    private void congufgUI() {
        if (loginPref.getIdProfile().equals(Constants.PROFILE_REPARTO)){
            txt_fv_nombre.setText("Repartidor:");
        }else if(loginPref.getIdProfile().equals(Constants.PROFILE_PRE_VENTA)){
            txt_fv_nombre.setText("Vendedor:");
        }
    }

    private void setData(EmitirComprobanteResponse.Comprobante comprobantePreventa) {

        txtNombreLocal.setText(comprobantePreventa.getCliente_destino());
        txt_tipo_comprobante.setText(comprobantePreventa.getEmision_tipo_doc());
        txt_cod_comprobante.setText(comprobantePreventa.getEmision_numero_doc());

        txt_total.setText("S/ ".concat(comprobantePreventa.getTotal()));
        txt_fecha_emision_cliente.setText(comprobantePreventa.getEmision_fecha());
        txt_moneda_cliente.setText(comprobantePreventa.getEmision_moneda());
        //txt_domicilio_cliente.setText(comprobantePreventa.getCliente_domicilio());
        txt_local_cliente.setText(comprobantePreventa.getCliente_destino());
        txt_nombre_cliente.setText(comprobantePreventa.getCliente_nombre());
        txt_ruc_cliente.setText(comprobantePreventa.getCliente_numerodoc());
        txt_direccion_cliente.setText(comprobantePreventa.getCliente_entrega());
        txt_nombre_comprobante.setText(comprobantePreventa.getCliente_tipodoc().concat(": "));
        txt_vendedor.setText(comprobantePreventa.getNombreVendedor());

        txt_tipo_pago.setText(comprobantePreventa.getTipoPago());
        //txt_medio_pago.setText(comprobantePreventa.getMedioPago());
        //txt_dias_credito.setText(comprobantePreventa.getDiasCredito());

        if (!comprobantePreventa.getFecha_entrega().isEmpty()) {
            ll_fecha_entrega.setVisibility(View.VISIBLE);
            txt_fecha_entrega_cliente.setText(comprobantePreventa.getFecha_entrega());
        } else {
            ll_fecha_entrega.setVisibility(View.GONE);
        }

//        if (!comprobantePreventa.getDescuentos().isEmpty()) {
//            ll_descuentos.setVisibility(View.VISIBLE);
//            txt_descuentos.setText(comprobantePreventa.getEmision_simbolo() + comprobantePreventa.getDescuentos());
//
//        } else {
//            ll_descuentos.setVisibility(View.GONE);
//        }
        if (!comprobantePreventa.getOper_gravadas().isEmpty()) {
            ll_ope_grab.setVisibility(View.VISIBLE);
            txt_op_grab.setText("S/ " + comprobantePreventa.getOper_gravadas());
        }

        if (!comprobantePreventa.getOper_gravadas().isEmpty()) {
            ll_ope_grab.setVisibility(View.VISIBLE);
            txt_op_grab.setText("S/ " + comprobantePreventa.getOper_gravadas());
        } else {
            ll_ope_grab.setVisibility(View.GONE);
            txt_op_grab.setVisibility(View.GONE);
        }
            if (!comprobantePreventa.getIgv().isEmpty()) {
            ll_igv.setVisibility(View.VISIBLE);
            txt_igv.setText("S/ " + comprobantePreventa.getIgv());
        } else {
            ll_igv.setVisibility(View.GONE);
            ll_igv.setVisibility(View.GONE);
        }

//        if (comprobantePreventa.getEstado() == 0) {
//            ll_bottom.setVisibility(View.VISIBLE);
//        } else if (comprobantePreventa.getEstado() == 1) {
//            btn_anular_comprobante.setVisibility(View.GONE);
//            btn_editar_pedido.setVisibility(View.GONE);
//            btn_emitir_comprobante.setVisibility(View.GONE);
//        } else if (comprobantePreventa.getEstado() == 2) {
//            btn_anular_comprobante.setVisibility(View.GONE);
//            btn_editar_pedido.setVisibility(View.GONE);
//            btn_emitir_comprobante.setVisibility(View.GONE);
//        }

        cargarPedidos(comprobantePreventa);

    }

    private void cargarPedidos(EmitirComprobanteResponse.Comprobante preventa) {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        rv_pedido_comprobante.setLayoutManager(layoutManager);
        rv_pedido_comprobante.setItemAnimator(new DefaultItemAnimator());

        emitirAdapter = new EmitirAdapter(getContext(), preventa.getDetalle());
        rv_pedido_comprobante.setAdapter(emitirAdapter);
        emitirAdapter.notifyDataSetChanged();
    }

}
