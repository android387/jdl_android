package com.jdl.distribution.ui.fragment.historial_despacho.model;

import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface EntregaHistorialService {
    @POST(Constants.WS_DESPACHO_HISTORIAL)
    Call<HistorialDespachoResponse> doEntrega(@Body String data);
}
