package com.jdl.distribution.ui.fragment.productos_frecuentes.dialog;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.dialog.base.BaseDialogFragment;
import com.jdl.distribution.R;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.ui.fragment.visita.model.ProductoFrecuenteResponse;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogDescuentoPrecio extends BaseDialogFragment implements View.OnClickListener {

    public static final String PRODUCTO_RESPONSE = "PRODUCTO_RESPONSE";
    public static final String CLIENTE_RESPONSE = "CLIENTE_RESPONSE";

    @BindView(R.id.img_close)
    ImageCardView img_close;
    @BindView(R.id.btn_aceptar)
    Button btn_aceptar;
    @BindView(R.id.txt_nombre_producto)
    TextView txt_nombre_producto;
    @BindView(R.id.txt_precio_normal)
    TextView txt_precio_normal;

    private Cliente cliente;
    private ProductoFrecuenteResponse.Product product;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_descuento_precio, container, false);
        setCancelable(false);
        ButterKnife.bind(this,view);
        init();
        return view;
    }

    private void init(){

        try {
            product = Parcels.unwrap(getArguments().getParcelable(PRODUCTO_RESPONSE));
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            cliente = Parcels.unwrap(getArguments().getParcelable(CLIENTE_RESPONSE));
        }catch (Exception e){
            e.printStackTrace();
        }

        setData();
        setListener();
    }

    private void setData() {
        txt_nombre_producto.setText(product.getNombre());
        txt_precio_normal.setText(product.getCostoSinDescuentos()+"");
    }

    private void setListener() {
        img_close.setOnClickListener(this);
        btn_aceptar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_close:
                dismiss();
                break;
            case R.id.btn_aceptar:
                dismiss();
                break;
        }
    }
}
