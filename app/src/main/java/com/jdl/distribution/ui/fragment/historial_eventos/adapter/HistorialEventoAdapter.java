package com.jdl.distribution.ui.fragment.historial_eventos.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.utils.Constants;
import com.developer.appsupport.utils.MyOutlineProvider;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.fragment.historial_eventos.model.HistorialEventoResponse;
import com.jdl.distribution.utils.adapter.AdapterBinder;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistorialEventoAdapter extends RecyclerView.Adapter<HistorialEventoAdapter.ViewHolder> {
    private static final String LOG_TAG = "HistorialEventoPres";
    Context context;
    ArrayList<HistorialEventoResponse.DetalleEvento> listEvento;
    EventoHistorialListener listener;

    public HistorialEventoAdapter(Context context, ArrayList<HistorialEventoResponse.DetalleEvento> listEvento, EventoHistorialListener listener) {
        this.context = context;
        this.listEvento = listEvento;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_evento_historial,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HistorialEventoResponse.DetalleEvento model = listEvento.get(position);

        holder.bindTo(model);
    }

    public int filterList(ArrayList<HistorialEventoResponse.DetalleEvento> listFiltroProgramados) {
        listEvento = listFiltroProgramados;
        notifyDataSetChanged();
        return listEvento.size();
    }

    @Override
    public int getItemCount() {
        return listEvento.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterBinder<HistorialEventoResponse.DetalleEvento>, View.OnClickListener {
        @BindView(R.id.txt_nombre_evento_historial)
        TextView txt_nombre_evento_historial;
        @BindView(R.id.txt_hora_evento_historial)
        TextView txt_hora_evento_historial;
        @BindView(R.id.txt_cliente_evento_historial)
        TextView txt_cliente_evento_historial;
        @BindView(R.id.txt_ruc_evento_historial)
        TextView txt_ruc_evento_historial;
        @BindView(R.id.txt_rs_evento_historial)
        TextView txt_rs_evento_historial;
        @BindView(R.id.ll_vendedor_evento)
        LinearLayout ll_vendedor_evento;
        @BindView(R.id.txt_vendedor_evento_historial)
        TextView txt_vendedor_evento_historial;
        @BindView(R.id.ll_rs)
        LinearLayout ll_rs;
        @BindView(R.id.ll_ruc)
        LinearLayout ll_ruc;
        @BindView(R.id.cv_evento_historial)
        CardView cv_evento_historial;
        @BindView(R.id.img_imp)
        ImageCardView img_imp;
        @BindView(R.id.img_edit)
        ImageCardView img_edit;
        @BindView(R.id.img_ojo)
        ImageCardView img_ojo;
        @BindView(R.id.txt_cpe)
        TextView txt_cpe;
        @BindView(R.id.ll_cpe)
        LinearLayout ll_cpe;
        @BindView(R.id.txt_factura)
        TextView txt_factura;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cv_evento_historial.setOutlineProvider(new MyOutlineProvider((int) Constants.convertDpiToPx(context, 10), 0.25f));
            }

            img_ojo.setOnClickListener(this);
            img_edit.setOnClickListener(this);
            img_imp.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.img_ojo:
                    listener.onItemClickListener(listEvento.get(getAdapterPosition()));
                    break;
                case R.id.img_edit:
                    listener.onItemClickListenerEditar(listEvento.get(getAdapterPosition()));
                    break;
                case R.id.img_imp:
                    listener.onItemClickListenerImprimir(listEvento.get(getAdapterPosition()));
                    break;
            }
        }

        @Override
        public void bindTo(@Nullable HistorialEventoResponse.DetalleEvento detalleEvento) {
            if (detalleEvento!=null) {

                txt_nombre_evento_historial.setText(detalleEvento.getNombreEvento());
                txt_hora_evento_historial.setText(detalleEvento.getHora());
                txt_cliente_evento_historial.setText(detalleEvento.getNombreCliente());
                txt_ruc_evento_historial.setText(detalleEvento.getRuc());
                txt_rs_evento_historial.setText(detalleEvento.getRs());
                txt_vendedor_evento_historial.setText(detalleEvento.getNomVen());

                txt_factura.setText(detalleEvento.getTipoDocumento());
                txt_cpe.setText(detalleEvento.getNombreDocumento());


                if (detalleEvento.getIdComprobante().isEmpty() || detalleEvento.getTipoComprobante().isEmpty()){
                    img_edit.setVisibility(View.GONE);
                    img_ojo.setVisibility(View.GONE);
                    img_imp.setVisibility(View.GONE);
                    cv_evento_historial.setEnabled(false);
                }else{
                    cv_evento_historial.setEnabled(true);

                    img_edit.setVisibility(View.VISIBLE);
                    img_ojo.setVisibility(View.VISIBLE);
                    img_imp.setVisibility(View.VISIBLE);

                    if (detalleEvento.getTipoComprobante().equals(String.valueOf(com.jdl.distribution.utils.Constants.TIPO_IMPRESION_PEDIDO))){
                        img_edit.setVisibility(View.VISIBLE);
                        img_ojo.setVisibility(View.VISIBLE);
                        img_imp.setVisibility(View.VISIBLE);
                    }else if (!detalleEvento.getTipoComprobante().equals(String.valueOf(com.jdl.distribution.utils.Constants.TIPO_IMPRESION_PEDIDO))){
                        img_ojo.setVisibility(View.VISIBLE);
                        img_imp.setVisibility(View.VISIBLE);
                        img_edit.setVisibility(View.GONE);
                    }
                }

                if (detalleEvento.getTipoDocumento().isEmpty()){
                    txt_factura.setVisibility(View.GONE);
                }else{
                    txt_factura.setVisibility(View.VISIBLE);
                }

                if (detalleEvento.getNombreDocumento().isEmpty()){
                    ll_cpe.setVisibility(View.GONE);
                }else {
                    ll_cpe.setVisibility(View.VISIBLE);
                }

                if (detalleEvento.getRuc().isEmpty()){
                    txt_cliente_evento_historial.setVisibility(View.GONE);
                    ll_ruc.setVisibility(View.GONE);
                    ll_rs.setVisibility(View.GONE);
                }else{
                    txt_cliente_evento_historial.setVisibility(View.VISIBLE);
                    ll_ruc.setVisibility(View.VISIBLE);
                    ll_rs.setVisibility(View.VISIBLE);
                }

//                if (new LoginPref(context).getIdProfile().equals(com.jdl.distribution.utils.Constants.PROFILE_SUPERVISOR)||
//                        new LoginPref(context).getIdProfile().equals(com.jdl.distribution.utils.Constants.PROFILE_ADMINISTRADOR)){
//                    ll_vendedor_evento.setVisibility(View.VISIBLE);
//                }else{
//                    ll_vendedor_evento.setVisibility(View.GONE);
//                }

            }
        }
    }

    public interface EventoHistorialListener {
        void onItemClickListener(HistorialEventoResponse.DetalleEvento evento);
        void onItemClickListenerEditar(HistorialEventoResponse.DetalleEvento evento);
        void onItemClickListenerImprimir(HistorialEventoResponse.DetalleEvento evento);
    }

}
