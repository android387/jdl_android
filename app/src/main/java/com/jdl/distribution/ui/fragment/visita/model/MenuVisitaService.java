package com.jdl.distribution.ui.fragment.visita.model;

import com.jdl.distribution.mvp.StatusMsgResponse;
import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MenuVisitaService {

    @POST(Constants.WS_PRODUCTOS_FRECUENTES)
    Call<ProductoFrecuenteResponse> doProductosFrecuentes(@Body String data);

    @POST(Constants.WS_PRODUCTOS_FRECUENTES)
    Call<EventoResponse> doValidarEvento(@Body String data);

    @POST(Constants.WS_CERRAR_VISITA)
    Call<StatusMsgResponse> doCerrarVisita(@Body String data);

    @POST(Constants.WS_ENTREGA_VALIDAR)
    Call<EntregaValidarResponse> getEntregaValidar(@Body String toString);

    @POST(Constants.WS_ENTREGA_TOTAL)
    Call<EntregaTotalResponse> getEntregaTotal(@Body String toString);
}
