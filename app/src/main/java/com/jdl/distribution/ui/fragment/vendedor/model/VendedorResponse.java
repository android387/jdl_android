package com.jdl.distribution.ui.fragment.vendedor.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcel;

import java.util.List;

public class VendedorResponse implements IStringNotNull<VendedorResponse>, StatusConverter {

    @Expose
    @SerializedName("listV")
    private List<Vendedor> listV;
    @Expose
    @SerializedName("status")
    private int status;
    @Expose
    @SerializedName("msg")
    private String msg;

    public List<Vendedor> getListV() {
        return listV;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Vendedor implements IStringNotNull<Vendedor> {

        @StringNotNull
        @Expose
        @SerializedName("idVen")
        private String idVendedor;
        @StringNotNull
        @Expose
        @SerializedName("name")
        private String nombre;
        @StringNotNull
        @Expose
        @SerializedName("phone")
        private String celular;
        @StringNotNull
        @Expose
        @SerializedName("mail")
        private String correo;
        @StringNotNull
        @Expose
        @SerializedName("dir")
        private String direccion;
        @StringNotNull
        @Expose
        @SerializedName("perfil")
        private String perfil;
        @StringNotNull
        @Expose
        @SerializedName("almacen")
        private String almacen;

        public String getIdVendedor() {
            return idVendedor;
        }

        public void setIdVendedor(String idVendedor) {
            this.idVendedor = idVendedor;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getCelular() {
            return celular;
        }

        public void setCelular(String celular) {
            this.celular = celular;
        }

        public String getCorreo() {
            return correo;
        }

        public void setCorreo(String correo) {
            this.correo = correo;
        }

        public String getDireccion() {
            return direccion;
        }

        public void setDireccion(String direccion) {
            this.direccion = direccion;
        }

        public String getPerfil() {
            return perfil;
        }

        public void setPerfil(String perfil) {
            this.perfil = perfil;
        }

        public String getAlmacen() {
            return almacen;
        }

        public void setAlmacen(String almacen) {
            this.almacen = almacen;
        }

        @Override
        public String toString() {
            return "Vendedor{" +
                    "idVendedor='" + idVendedor + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", celular='" + celular + '\'' +
                    ", correo='" + correo + '\'' +
                    ", direccion='" + direccion + '\'' +
                    ", perfil='" + perfil + '\'' +
                    ", almacen='" + almacen + '\'' +
                    '}';
        }

        @Override
        public Vendedor getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Override
    public VendedorResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Override
    public String toString() {
        return "VendedorResponse{" +
                "listV=" + listV +
                ", status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }
}
