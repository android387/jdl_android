package com.jdl.distribution.ui.fragment.historial_pedidos.model;

import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PedidoService {
    @POST(Constants.WS_PEDIDO_HISTORIAL)
    Call<PedidoHistorialResponse> doPedido(@Body String data);

}
