package com.jdl.distribution.ui.fragment.devolucion.devolucion_parcial.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.parceler.Parcel;

import java.util.List;

public class EditarListaDevolucionResponse implements IStringNotNull<EditarListaDevolucionResponse>, StatusConverter {

    @StringNotNull
    @Expose
    @SerializedName("msg")
    private String msg;
    @StringNotNull
    @Expose
    @SerializedName("idDoc")
    private String idDoc;
    @StringNotNull
    @Expose
    @SerializedName("nomDoc")
    private String nombreDoc;
    @Expose
    @SerializedName("listDet")
    private List<Detalle> listD;
    @Expose
    @SerializedName("status")
    private int status;

    public String getIdDoc() {
        return idDoc;
    }

    public String getNombreDoc() {
        return nombreDoc;
    }

    public String getMsg() {
        return msg;
    }

    public List<Detalle> getListD() {
        return listD;
    }

    public int getStatus() {
        return status;
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Detalle implements IStringNotNull<Detalle> {
        @Expose
        @SerializedName("cod")
        private String codigo;
        @Expose
        @SerializedName("nom")
        private String nombre;
        @Expose
        @SerializedName("cant")
        private String cantidad;
        @Expose
        @SerializedName("idPro")
        private String idPro;

        private boolean isCkecked=false;

        private boolean hasFocus=false;

        private String nuevaCantidad="";

        public boolean isHasFocus() {
            return hasFocus;
        }

        public void setHasFocus(boolean hasFocus) {
            this.hasFocus = hasFocus;
        }

        public String getNuevaCantidad() {
            return nuevaCantidad;
        }

        public void setNuevaCantidad(String nuevaCantidad) {
            this.nuevaCantidad = nuevaCantidad;
        }

        public boolean isCkecked() {
            return isCkecked;
        }

        public void setCkecked(boolean ckecked) {
            isCkecked = ckecked;
        }

        public String getCodigo() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getCantidad() {
            return cantidad;
        }

        public void setCantidad(String cantidad) {
            this.cantidad = cantidad;
        }

        public String getIdPro() {
            return idPro;
        }

        public void setIdPro(String idPro) {
            this.idPro = idPro;
        }

        @Override
        public String toString() {
            return "Detalle{" +
                    "codigo='" + codigo + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", cantidad='" + cantidad + '\'' +
                    ", idPro='" + idPro + '\'' +
                    '}';
        }

        @Override
        public Detalle getWithStringNotNull() {
            return getWithStringNotNull(this);
        }


    }

    @Override
    public EditarListaDevolucionResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }

    @Override
    public String toString() {
        return "EditListDevolucionResponse{" +
                "msg='" + msg + '\'' +
                ", idDoc='" + idDoc + '\'' +
                ", listD=" + listD +
                ", status=" + status +
                '}';
    }
}
