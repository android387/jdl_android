package com.jdl.distribution.ui.fragment.emitir_comprobante.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jdl.distribution.mvp.Status;
import com.jdl.distribution.mvp.StatusConverter;

import org.parceler.Parcel;

import java.util.List;

@Parcel(Parcel.Serialization.BEAN)
public class EmitirComprobanteResponse implements IStringNotNull<EmitirComprobanteResponse>, StatusConverter {

    @Expose
    @SerializedName("Pedido")
    private Comprobante data;
    @Expose
    @SerializedName("status")
    private int status;
    @Expose
    @SerializedName("msg")
    private String msg;

    public Comprobante getData() {
        return data;
    }

    public void setData(Comprobante data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }


    @Parcel(Parcel.Serialization.BEAN)
    public static class Comprobante implements IStringNotNull<Comprobante>{
        @StringNotNull
        @Expose
        @SerializedName("pe_id")
        private String idPedido;
        @Expose
        @SerializedName("detalle")
        private List<DetallePreventa> detalle;
        @StringNotNull
        @Expose
        @SerializedName("fecha_entrega")
        private String fecha_entrega;
        @StringNotNull
        @Expose
        @SerializedName("emision_fecha")
        private String emision_fecha;
        @StringNotNull
        @Expose
        @SerializedName("total")
        private String total;
//        @StringNotNull
//        @Expose
//        @SerializedName("descuentos")
//        private String descuentos;
        @StringNotNull
        @Expose
        @SerializedName("emision_moneda")
        private String emision_moneda;
        @StringNotNull
        @Expose
        @SerializedName("cliente_direccion")
        private String cliente_entrega;
        @StringNotNull
        @Expose
        @SerializedName("cliente_local")
        private String cliente_destino;
//        @StringNotNull
//        @Expose
//        @SerializedName("cliente_domicilio")
//        private String cliente_domicilio;
        @StringNotNull
        @Expose
        @SerializedName("cliente_nombre")
        private String cliente_nombre;
        @StringNotNull
        @Expose
        @SerializedName("cliente_numerodoc")
        private String cliente_numerodoc;
        @StringNotNull
        @Expose
        @SerializedName("cliente_tipodoc")
        private String cliente_tipodoc;
        @StringNotNull
        @Expose
        @SerializedName("emision_numero_doc")
        private String emision_numero_doc;
        @StringNotNull
        @Expose
        @SerializedName("emision_tipo_doc")
        private String emision_tipo_doc;
        @StringNotNull
        @Expose
        @SerializedName("igv")
        private String igv;
        @StringNotNull
        @Expose
        @SerializedName("vendedor")
        private String nombreVendedor;
        @StringNotNull
        @Expose
        @SerializedName("tipoPago")
        private String tipoPago;
        @StringNotNull
        @Expose
        @SerializedName("oper_gravadas")
        private String oper_gravadas;
//        @StringNotNull
//        @Expose
//        @SerializedName("medioPago")
//        private String medioPago;
        @StringNotNull
        @Expose
        @SerializedName("tipocom")
        private String tipoComprobante;
        @Expose
        @SerializedName("estado")
        private int estado;

        public String getIdPedido() {
            return idPedido;
        }

        public String getFecha_entrega() {
            return fecha_entrega;
        }

        public String getOper_gravadas() {
            return oper_gravadas;
        }

        public String getEmision_fecha() {
            return emision_fecha;
        }

        public String getTotal() {
            return total;
        }

        public String getEmision_moneda() {
            return emision_moneda;
        }

        public String getCliente_entrega() {
            return cliente_entrega;
        }

        public String getCliente_destino() {
            return cliente_destino;
        }

        public String getCliente_nombre() {
            return cliente_nombre;
        }

        public String getCliente_numerodoc() {
            return cliente_numerodoc;
        }

        public String getCliente_tipodoc() {
            return cliente_tipodoc;
        }

        public String getEmision_numero_doc() {
            return emision_numero_doc;
        }

        public String getEmision_tipo_doc() {
            return emision_tipo_doc;
        }

        public String getIgv() {
            return igv;
        }

        public String getNombreVendedor() {
            return nombreVendedor;
        }

        public String getTipoPago() {
            return tipoPago;
        }

        public String getTipoComprobante() {
            return tipoComprobante;
        }

        public int getEstado() {
            return estado;
        }

        @Override
        public String toString() {
            return "Comprobante{" +
                    "idPedido='" + idPedido + '\'' +
                    ", detalle=" + detalle +
                    ", fecha_entrega='" + fecha_entrega + '\'' +
                    ", emision_fecha='" + emision_fecha + '\'' +
                    ", total='" + total + '\'' +
                    ", emision_moneda='" + emision_moneda + '\'' +
                    ", cliente_entrega='" + cliente_entrega + '\'' +
                    ", cliente_destino='" + cliente_destino + '\'' +
                    ", cliente_nombre='" + cliente_nombre + '\'' +
                    ", cliente_numerodoc='" + cliente_numerodoc + '\'' +
                    ", cliente_tipodoc='" + cliente_tipodoc + '\'' +
                    ", emision_numero_doc='" + emision_numero_doc + '\'' +
                    ", emision_tipo_doc='" + emision_tipo_doc + '\'' +
                    ", igv='" + igv + '\'' +
                    ", nombreVendedor='" + nombreVendedor + '\'' +
                    ", tipoPago='" + tipoPago + '\'' +
                    ", tipoComprobante='" + tipoComprobante + '\'' +
                    ", estado=" + estado +
                    '}';
        }

        public List<DetallePreventa> getDetalle() {
            return detalle;
        }

        public void setDetalle(List<DetallePreventa> detalle) {
            this.detalle = detalle;
        }

        @Override
        public Comprobante getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class DetallePreventa implements IStringNotNull<DetallePreventa> {

        @StringNotNull
        @Expose
        @SerializedName("codigo")
        private String codigo;
        @StringNotNull
        @Expose
        @SerializedName("nombre")
        private String nombre;
        @StringNotNull
        @Expose
        @SerializedName("cant")
        private String cantidad;
        @StringNotNull
        @Expose
        @SerializedName("costo")
        private String costos;
        @StringNotNull
        @Expose
        @SerializedName("venta")
        private String venta;
        @StringNotNull
        @Expose
        @SerializedName("subttotal")
        private String subttotal;

        public String getCodigo() {
            return codigo;
        }

        public String getNombre() {
            return nombre;
        }

        public String getCantidad() {
            return cantidad;
        }

        public String getCostos() {
            return costos;
        }

        public String getVenta() {
            return venta;
        }

        public String getSubttotal() {
            return subttotal;
        }

        @Override
        public String toString() {
            return "DetallePreventa{" +
                    "codigo='" + codigo + '\'' +
                    ", nombre='" + nombre + '\'' +
                    ", cantidad='" + cantidad + '\'' +
                    ", costos='" + costos + '\'' +
                    ", venta='" + venta + '\'' +
                    ", subttotal='" + subttotal + '\'' +
                    '}';
        }

        @Override
        public DetallePreventa getWithStringNotNull() {
            return getWithStringNotNull(this);
        }
    }


    @Override
    public EmitirComprobanteResponse getWithStringNotNull() {
        return getWithStringNotNull(this);
    }

    @Override
    public Status getStatusEnum() {
        return getStatusEnum(getStatus());
    }
}
