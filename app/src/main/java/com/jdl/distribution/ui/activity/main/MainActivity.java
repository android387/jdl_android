package com.jdl.distribution.ui.activity.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.developer.appsupport.ui.activity.BaseActivity;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.utils.gps.GpsStatusDetector;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.AppPref;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.ui.activity.bluetooth.BluetoothActivity;
import com.jdl.distribution.ui.activity.login.LoginActivity;
import com.jdl.distribution.ui.activity.main.ContenedorHistorial.ContenedorHistorialFragment;
import com.jdl.distribution.ui.dialog_fragment.dialog_advertencia.DialogAdvertencia;
import com.jdl.distribution.ui.fragment.clientes.ClientesFragment;
import com.jdl.distribution.ui.fragment.emitir_comprobante.EmitirComprobanteFragment;
import com.jdl.distribution.ui.fragment.mapa.MapaClienteFragment;
import com.jdl.distribution.ui.fragment.productos.ProductosFragment;
import com.jdl.distribution.ui.fragment.programados.ProgramadosFragment;
import com.jdl.distribution.ui.fragment.vendedor.VendedorFragment;
import com.jdl.distribution.ui.fragment.visita.VisitaFragment;
import com.jdl.distribution.utils.Constants;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements View.OnClickListener, OnSuccessListener<Location>, GpsStatusDetector.GpsStatusDetectorCallBack {

    public static final String LOG_TAG = MainActivity.class.getSimpleName();

    private GpsStatusDetector gpsStatusDetector;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationCallback locationCallback;
    public Location location;
    private String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    private AppPref appPref;
    private LoginPref loginPref;
    private DialogFragment dialogAdvertencia;
    private Timer timer;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.navigation_view)
    NavigationView navigation_view;
    @BindView(R.id.navigation_view_account)
    NavigationView navigation_view_account;
    @BindView(R.id.icv_open_menu)
    ImageCardView icvOpenMenu;
    @BindView(R.id.icv_open_menu_account)
    ImageCardView icv_open_menu_account;

    @BindView(R.id.txt_programacion_nav)
    LinearLayout txt_programacion_nav;
    @BindView(R.id.txt_cliente_nav)
    LinearLayout txt_cliente_nav;
    @BindView(R.id.txt_producto_nav)
    LinearLayout txt_producto_nav;
    @BindView(R.id.txt_mapa_nav)
    LinearLayout txt_mapa_nav;
    @BindView(R.id.txt_vendedor_nav)
    LinearLayout txt_vendedor_nav;
    @BindView(R.id.txt_cerrar_sesion_nav)
    LinearLayout txt_cerrar_sesion_nav;
    @BindView(R.id.txt_historial_nav)
    LinearLayout txt_historial_nav;
    @BindView(R.id.txt_nombre)
    TextView txt_nombre;
    @BindView(R.id.txt_dni)
    TextView txt_dni;
    @BindView(R.id.txt_email)
    TextView txt_email;
    @BindView(R.id.txt_perfil_header)
    TextView txt_perfil_header;
    @BindView(R.id.ll_config_impresora)
    LinearLayout ll_config_impresora;

    @BindView(R.id.img_header)
    ImageView img_header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this, this);
        init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {

        appPref = new AppPref(this);
        loginPref = new LoginPref(this);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        gpsStatusDetector = new GpsStatusDetector(this);

        initDialog(savedInstanceState);
        cargarProgramados(savedInstanceState, null);
        initLocationCallback();
        checkLocations();
        setListener();
        setData();

    }

    private void setData() {
        img_header.setPadding(20, 20, 20, 20);
        Glide.with(this)
                .load(R.drawable.ic_grocery)
                .into(img_header);

        switch (loginPref.getIdProfile()) {
            case Constants.PROFILE_PRE_VENTA:
                txt_perfil_header.setText("Pre-Venta");
                break;
            case Constants.PROFILE_REPARTO:
                txt_perfil_header.setText("Repartidor");
                txt_cliente_nav.setVisibility(View.GONE);
                break;
        }


    }

    private void initDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            dialogAdvertencia = (DialogAdvertencia) getSupportFragmentManager().findFragmentByTag(DialogAdvertencia.class.getSimpleName());

            if (dialogAdvertencia == null)
                dialogAdvertencia = new DialogAdvertencia();

        } else {
            dialogAdvertencia = new DialogAdvertencia();
        }
    }

    private void setListener() {
        icvOpenMenu.setOnClickListener(this);
        icv_open_menu_account.setOnClickListener(this);
        txt_programacion_nav.setOnClickListener(this);
        txt_cliente_nav.setOnClickListener(this);
        txt_producto_nav.setOnClickListener(this);
        txt_mapa_nav.setOnClickListener(this);
        txt_vendedor_nav.setOnClickListener(this);
        txt_cerrar_sesion_nav.setOnClickListener(this);
        txt_historial_nav.setOnClickListener(this);

        txt_nombre.setText(loginPref.getNombres());
        txt_dni.setText(loginPref.getDNI());
        txt_email.setText(loginPref.getCorreo());

        ll_config_impresora.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        for (String permision : permissions) {
            if (permision.equals(Manifest.permission.ACCESS_COARSE_LOCATION)
                    || permision.equals(Manifest.permission.ACCESS_FINE_LOCATION))
                checkLocations();
        }

        if (requestCode == Constants.REQUEST_CODE_PERMISION_CAMERA) {
            for (String permision : permissions) {
                if (permision.equals(Manifest.permission.CAMERA)
                        || permision.equals(Manifest.permission.READ_EXTERNAL_STORAGE)
                        || permision.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                    checkPermissionCamera();
            }
        }

    }

    private void checkPermissionCamera() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.permissionsCamera[0])) {

                ActivityCompat.requestPermissions(this, Constants.permissionsCamera, Constants.REQUEST_CODE_PERMISION_CAMERA);

            } else {

                ActivityCompat.requestPermissions(this, Constants.permissionsCamera, Constants.REQUEST_CODE_PERMISION_CAMERA);

            }

            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    private void cancelTimer() {
        try {
            timer.cancel();
            timer.purge();
            timer = null;
            Log.i("Timer", "Timer cancelado...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
//        ServiceUtils.startService(this, MqttService.class);

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!loginPref.getUid().isEmpty()) {
                    createLocationRequest();
                }
            }
        }, 0, (30000));

        new AppPref(this).setActivity(MainActivity.class.getSimpleName());

    }

    @Override
    public void onBackPressed() {

        if (drawer_layout.isDrawerOpen(navigation_view)) {
            drawer_layout.closeDrawer(navigation_view); //OPEN Nav Drawer!
        }
        if (drawer_layout.isDrawerOpen(navigation_view_account)) {
            drawer_layout.closeDrawer(navigation_view_account); //OPEN Nav Drawer!
        }


        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_content);

        Fragment nameFragment[] = {
                new ProgramadosFragment(),
                new ClientesFragment(),
                new ProductosFragment(),
                new MapaClienteFragment(),
                new VendedorFragment(),
                new ContenedorHistorialFragment()
        };

        for (int i = 0; i < nameFragment.length; i++) {
            if (fragment.getClass().getSimpleName().equals(nameFragment[i].getClass().getSimpleName())) {
                Log.d("FRAGMENT", fragment.getClass().getSimpleName() + " OTRO: " + nameFragment[i].getClass().getSimpleName());
                dialogAdvertencia.showNow(getSupportFragmentManager(), DialogAdvertencia.class.getSimpleName());
                return;
            }
        }

        if (fragment instanceof EmitirComprobanteFragment && EmitirComprobanteFragment.OPEN == false) {
            //EmitirComprobanteFragment.OPEN = true;
            String name = getSupportFragmentManager().getBackStackEntryAt(0).getName();
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            cargarProgramados(null, name);
            return;
        }

        if (fragment instanceof VisitaFragment && VisitaFragment.OPEN) {
            dialogAdvertencia.showNow(getSupportFragmentManager(), DialogAdvertencia.class.getSimpleName());
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
        new AppPref(this).setActivity("");
    }

    private void initLocationCallback() {

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    MainActivity.this.location = location;
                }

                if (location == null)
                    return;
            }

        };


    }

    private void checkLocations() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])) {

                ActivityCompat.requestPermissions(this, permissions, Constants.REQUEST_CODE_PERMISION_LOCATION);

            } else {

                ActivityCompat.requestPermissions(this, permissions, Constants.REQUEST_CODE_PERMISION_LOCATION);

            }

            return;
        }


        //fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, this);

    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.icv_open_menu:
//                drawer_layout.openDrawer(GravityCompat.START);
                drawer_layout.openDrawer(navigation_view);
                drawer_layout.closeDrawer(navigation_view_account);
                break;
            case R.id.icv_open_menu_account:
                drawer_layout.openDrawer(navigation_view_account);
                drawer_layout.closeDrawer(navigation_view);
                break;
            case R.id.txt_programacion_nav:
                cargarProgramados(null, null);
                closeDrawer();
                break;
            case R.id.txt_cliente_nav:
                Fragment clientsFragment = new ClientesFragment();
                transaction.addToBackStack(ClientesFragment.class.getSimpleName());
                transaction.replace(R.id.main_content, clientsFragment, ClientesFragment.class.getSimpleName());
                transaction.commit();
                closeDrawer();
                break;
            case R.id.txt_producto_nav:
                Fragment productoFragment = new ProductosFragment();
                transaction.addToBackStack(ClientesFragment.class.getSimpleName());
                transaction.replace(R.id.main_content, productoFragment, ProductosFragment.class.getSimpleName());
                transaction.commit();
                closeDrawer();
                break;
            case R.id.txt_mapa_nav:
                Fragment mapaFragment = new MapaClienteFragment();
                transaction.addToBackStack(ClientesFragment.class.getSimpleName());
                transaction.replace(R.id.main_content, mapaFragment, MapaClienteFragment.class.getSimpleName());
                transaction.commit();
                closeDrawer();
                break;
            case R.id.txt_vendedor_nav:
                Fragment vendedorFragment = new VendedorFragment();
                transaction.addToBackStack(ClientesFragment.class.getSimpleName());
                transaction.replace(R.id.main_content, vendedorFragment, VendedorFragment.class.getSimpleName());
                transaction.commit();
                closeDrawer();
                break;
            case R.id.txt_historial_nav:
                Fragment historialFragment = new ContenedorHistorialFragment();
                transaction.addToBackStack(ClientesFragment.class.getSimpleName());
                transaction.replace(R.id.main_content, historialFragment, ContenedorHistorialFragment.class.getSimpleName());
                transaction.commit();
                closeDrawer();
                break;
            case R.id.txt_cerrar_sesion_nav:
                loginPref.logout();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                closeDrawer();
                break;


            case R.id.ll_config_impresora:
                startActivity(new Intent(this, BluetoothActivity.class));
                break;
        }
    }

    private void closeDrawer() {
        drawer_layout.closeDrawer(navigation_view);
        drawer_layout.closeDrawer(navigation_view_account);
    }

    @Override
    public void onGpsSettingStatus(boolean enabled) {

    }

    @Override
    public void onGpsAlertCanceledByUser() {

    }

    private void stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    protected void createLocationRequest() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, locationCallback, Looper.getMainLooper());
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, this);
    }

    @Override
    public void onSuccess(Location location) {
        if (location == null) {
            if (!com.developer.appsupport.utils.Constants.isGpsEnabled(this)) {
                gpsStatusDetector.checkGpsStatus();
            }
            return;
        }

        this.location = location;

        appPref.setLatitude(String.valueOf(location.getLatitude()));
        appPref.setLongitude(String.valueOf(location.getLongitude()));
        Log.d("LATLON",appPref.getLatitude()+","+appPref.getLongitude());
    }

    private void cargarProgramados(Bundle savedInstanceState, String name) {

        if (savedInstanceState == null) {

            ProgramadosFragment fragment = new ProgramadosFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(name)
                    .replace(R.id.main_content, fragment, ProgramadosFragment.class.getSimpleName())
                    .commit();
        }
    }

}
