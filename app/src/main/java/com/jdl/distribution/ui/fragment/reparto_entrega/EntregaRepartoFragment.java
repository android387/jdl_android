package com.jdl.distribution.ui.fragment.reparto_entrega;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.appsupport.annotation.onetime.OneTime;
import com.developer.appsupport.annotation.onetime.OneTimer;
import com.developer.appsupport.ui.custom.ImageCardView;
import com.developer.appsupport.ui.custom.ProgressButton;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.utils.JSONGenerator;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ParcelerArgsBundler;
import com.jdl.distribution.R;
import com.jdl.distribution.data.preference.LoginPref;
import com.jdl.distribution.fragmentargs.ParcelerArrayListBundler;
import com.jdl.distribution.mvp.model.Cliente;
import com.jdl.distribution.ui.fragment.base.BaseMvpFragment;
import com.jdl.distribution.ui.fragment.emitir_comprobante.EmitirComprobanteFragment;
import com.jdl.distribution.ui.fragment.emitir_comprobante.EmitirComprobanteFragmentBuilder;
import com.jdl.distribution.ui.fragment.emitir_comprobante.model.EmitirComprobanteResponse;
import com.jdl.distribution.ui.fragment.productos_frecuentes.ProductosFrecuentesFragment;
import com.jdl.distribution.ui.fragment.reparto_entrega.adapter.EntregaAdapter;
import com.jdl.distribution.ui.fragment.reparto_entrega.dialog_motivp.DialogMotivo;
import com.jdl.distribution.ui.fragment.reparto_entrega.model.EntregaResponse;
import com.jdl.distribution.ui.fragment.reparto_entrega.presenter.EntregaPresenter;
import com.jdl.distribution.ui.fragment.reparto_entrega.view.EntregaView;
import com.jdl.distribution.ui.fragment.visita.model.EntregaTotalResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@FragmentWithArgs
public class EntregaRepartoFragment extends BaseMvpFragment<EntregaView, EntregaPresenter>
        implements View.OnClickListener, EntregaView {

    @Arg
    String idVisita;
    @Arg(bundler = ParcelerArgsBundler.class)
    Cliente cliente;
    /*@Arg(bundler = ParcelerArgsBundler.class)
    EntregaTotalResponse response;*/
    @Arg(bundler = ParcelerArrayListBundler.class)
    List<EntregaTotalResponse.Entrega> response;

    @BindView(R.id.rv_entrega)
    RecyclerView rv_entrega;
    @BindView(R.id.chk_todos)
    CheckBox chk_todos;
    @BindView(R.id.img_back)
    ImageCardView img_back;
    @BindView(R.id.btn_entrega)
    ProgressButton btn_entrega;
    @BindView(R.id.txt_nombre_cliente)
    TextView txt_nombre_cliente;

    private LoginPref loginPref;

    private DialogFragment dialogMotivo;
    @OneTime
    private ProgressButton btnEnviar;

    private EntregaAdapter adapter;
    private double total;
    private List<DetalleEntrega> listEntrega = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_entrega_reparto, container, false);
        FragmentArgs.inject(this);
        ButterKnife.bind(this, view);
        init(savedInstanceState);
        return view;
    }

    private void init(Bundle savedInstanceState) {
        loginPref = new LoginPref(getContext());
        initDialog(savedInstanceState);

        txt_nombre_cliente.setText(cliente.getNombre());


        for (int i = 0; i < response.size(); i++) {
            DetalleEntrega entrega = new DetalleEntrega(response.get(i).getIdPro(), Integer.valueOf(response.get(i).getCantidad()));
            total = total + Double.valueOf(response.get(i).getImporte());
            listEntrega.add(entrega);
        }

        setAdapter();
        setListener();
    }

    private void initDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            dialogMotivo = (DialogMotivo) getChildFragmentManager().findFragmentByTag(DialogMotivo.class.getSimpleName());

            if (dialogMotivo == null)
                dialogMotivo = new DialogMotivo();

        } else {
            dialogMotivo = new DialogMotivo();
        }
    }

    private void setAdapter() {
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        rv_entrega.setLayoutManager(layoutManager);
        rv_entrega.setItemAnimator(new DefaultItemAnimator());
        adapter = new EntregaAdapter(getContext(), response);
        rv_entrega.setAdapter(adapter);

    }

    private void setListener() {
        img_back.setOnClickListener(this);
        btn_entrega.setOnClickListener(this);
        btn_entrega.setOneTimerSupport(true);

        ((DialogMotivo) dialogMotivo).setListenerMotivo(button -> {
            btnEnviar = button;

            JSONGenerator.getNewInstance(getActivity())
                    .requireInternet(true)
                    .put("id_perfil", loginPref.getIdProfile())
                    .put("id_usuario", loginPref.getUid())
                    .put("id_cliente", cliente.getIdcli())
                    .put("total", total)
                    .put("id_vis", idVisita)
                    .put("id_vta", response.get(0).getIdVenta())
                    .put("detalle", listEntrega)
                    .operate(jsonObject -> {
                        OneTimer.subscribe(this, btnEnviar);
                        getPresenter().doEntrega(jsonObject);
                    });
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                getActivity().onBackPressed();
                break;
            case R.id.btn_entrega:
                if (dialogMotivo.isResumed())
                    return;
                Bundle bundle = new Bundle();

                dialogMotivo.showNow(getChildFragmentManager(), DialogMotivo.class.getSimpleName());
                break;
        }
    }

    @Override
    public EntregaPresenter createPresenter() {
        return new EntregaPresenter(getContext());
    }

    @Override
    public void onEntregarSuccess(EntregaResponse response) {
        if (getActivity() == null) return;

        JSONGenerator.getNewInstance(getActivity())
                .requireInternet(true)
                .put("id_ped", response.getIdEntrega())
                .put("id_pf", new LoginPref(getContext()).getIdProfile())
                .put("id_fv", new LoginPref(getContext()).getUid())
                .operate(getPresenter()::doEmitirComprobantePreventaRegister);

        OneTimer.unsubscribe(this, btnEnviar);
    }

    @Override
    public void onEntregarFail(String mensaje) {
        if (getActivity() == null) return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btnEnviar);
    }

    @Override
    public void onEmitirComprobanteSuccess(EmitirComprobanteResponse response) {
        if (getActivity() == null)
            return;

        EmitirComprobanteFragment.OPEN = false;

        Fragment signupFragment = new EmitirComprobanteFragmentBuilder(response.getData()).build();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(EmitirComprobanteFragment.class.getSimpleName());
        transaction.setCustomAnimations(R.anim.left_in, R.anim.left_out, R.anim.right_in, R.anim.right_out);
        transaction.replace(R.id.main_content, signupFragment);
        transaction.commitAllowingStateLoss();

        OneTimer.unsubscribe(EntregaRepartoFragment.this, btnEnviar);
    }

    @Override
    public void onEmitirComprobanteSFail(String mensaje) {
        if (getActivity() == null)
            return;
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
        OneTimer.unsubscribe(this, btnEnviar);
    }

    public static class DetalleEntrega {
        String ent_producto_id;
        int ent_producto_cant;

        public DetalleEntrega(String ent_producto_id, int ent_producto_cant) {
            this.ent_producto_id = ent_producto_id;
            this.ent_producto_cant = ent_producto_cant;
        }
    }

}
