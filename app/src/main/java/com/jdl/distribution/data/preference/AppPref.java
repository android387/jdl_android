package com.jdl.distribution.data.preference;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPref {

    private final String SHARED_PREFERENCES_NAME = "APP_PREF_JDL_PREFERENCES";

    private final String FRAGMENT = "FRAGMENT_PREFERENCES";
    private final String ACTIVITY = "ACTIVITY_PREFERENCES";
    private final String LATITUDE = "LATITUDE_PREFERENCES";
    private final String LONGITUDE = "LONGITUDE_PREFERENCES";
    private final String AVAILABILITY = "AVAILABILITY_PREFERENCES";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public AppPref(Context context) {
        preferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, context.MODE_PRIVATE);
        editor = preferences.edit();
    }


    public void logout(){
        setActivity("");
        setFragment("");
    }


    public String getFragment() {
        return preferences.getString(FRAGMENT, "");
    }

    public void setFragment(String estado) {
        editor.putString(FRAGMENT,estado).commit();
    }


    public String getActivity() {
        return preferences.getString(ACTIVITY, "");
    }

    public void setActivity(String estado) {
        editor.putString(ACTIVITY,estado).commit();
    }

    public String getLatitude() {
        return preferences.getString(LATITUDE, "");
    }

    public void setLatitude(String lat) {
        editor.putString(LATITUDE,lat).commit();
    }


    public String getLongitude() {
        return preferences.getString(LONGITUDE, "");
    }

    public void setLongitude(String lng) {
        editor.putString(LONGITUDE,lng).commit();
    }


    public String getAvailability() {
        return preferences.getString(AVAILABILITY, "");
    }

    public void setAvailability(String availability) {
        editor.putString(AVAILABILITY,availability).commit();
    }

}
