package com.jdl.distribution.data.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.jdl.distribution.ui.activity.login.model.LoginResponse;

public class LoginPref {

    private final String SHARED_PREFERENCES_NAME = "LOGIN_SPRYSALES_VENDEDOR_PREFERENCES";
    private final String UID = "UID_PREFERENCES";
    private final String ID_PROFILE = "ID_PROFILE_PREFERENCES";
    private final String NOMBRES = "NOMBRES_PREFERENCES";
    private final String APELLIDOS = "APELLIDOS_PREFERENCES";
    private final String DNI = "DNI_PREFERENCES";
    private final String CORREO = "CORREO_PREFERENCES";
    private final String TOKEN = "TOKEN_PREFERENCES";



    private SharedPreferences.Editor editor;
    private SharedPreferences preferences;

    public LoginPref(Context context) {
        this.preferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        this.editor = this.preferences.edit();
    }

    public String getUid()
    {
        return this.preferences.getString(UID, "");
    }

    public void setUid(String uid) {
        this.editor.putString(UID, uid).commit();
    }



    public String getIdProfile()
    {
        return this.preferences.getString(ID_PROFILE, "");
    }

    public void setIdProfile(String uid) {
        this.editor.putString(ID_PROFILE, uid).commit();
    }



    public String getNombres()
    {
        return this.preferences.getString(NOMBRES, "");
    }

    public void setNombres(String nombres) {
        this.editor.putString(NOMBRES, nombres).commit();
    }



    public String getApellidos() {
        return this.preferences.getString(APELLIDOS, "");
    }

    public void setApellidos(String apellidos) {
        this.editor.putString(APELLIDOS, apellidos).commit();
    }



    public String getDNI() { return this.preferences.getString(DNI, ""); }

    public void setDNI(String dni) { this.editor.putString(DNI, dni).commit(); }



    public String getCorreo()
    {
        return this.preferences.getString(CORREO, "");
    }

    public void setCorreo(String correo) {
        this.editor.putString(CORREO, correo).commit();
    }

    public String getToken() {
        return preferences.getString(TOKEN, "");
    }

    public void setToken(String token) {
        editor.putString(TOKEN, token).commit();
    }

    /**
     * @return boolean true if login success else false
     * */
    public boolean login(LoginResponse loginResponse) {

        if (loginResponse.getUsuario()==null)
            return false;

        loginResponse.getUsuario().getWithStringNotNull();

        setUid(loginResponse.getUsuario().getIdUsuario());
        setNombres(loginResponse.getUsuario().getNombre());
        setApellidos(loginResponse.getUsuario().getApellido());
        setCorreo(loginResponse.getUsuario().getCorreo());
        setIdProfile(loginResponse.getUsuario().getIdPerfil());
        setDNI(loginResponse.getUsuario().getDocumento());

        return true;

    }

    public void logout() {
        setUid("");
        setIdProfile("");
        setNombres("");
        setApellidos("");
        setDNI("");
        setCorreo("");
    }

}
