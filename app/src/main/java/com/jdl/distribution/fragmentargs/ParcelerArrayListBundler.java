package com.jdl.distribution.fragmentargs;

import android.os.Bundle;
import android.os.Parcelable;

import com.hannesdorfmann.fragmentargs.bundler.ArgsBundler;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class ParcelerArrayListBundler implements ArgsBundler<List<?>> {

    @Override
    public void put(String key, List<?> value, Bundle bundle) {

        ArrayList<Parcelable> list = new ArrayList<>();
        for (Object o : value){

            list.add(Parcels.wrap(o));

        }

        bundle.putParcelableArrayList(key, list);

    }

    @Override
    public <V extends List<?>> V get(String key, Bundle bundle) {

        List<?> list = new ArrayList<>();
        for (Parcelable o : bundle.getParcelableArrayList(key)){

            list.add(Parcels.unwrap(o));

        }

        return (V) list;
    }

}
