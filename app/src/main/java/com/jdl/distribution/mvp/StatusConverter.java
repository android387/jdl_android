package com.jdl.distribution.mvp;

import com.developer.appsupport.utils.Constants;

public interface StatusConverter {

    Status getStatusEnum();

    default Status getStatusEnum(int status){
        if (status== Constants.WS_STATUS_SUCCESS)
            return Status.SUCCESS;
        else return Status.FAIL;
    }

}
