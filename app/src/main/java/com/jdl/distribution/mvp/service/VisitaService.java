package com.jdl.distribution.mvp.service;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.jdl.distribution.mvp.model.ClienteResponse;
import com.jdl.distribution.ui.fragment.programados.model.InitVisitResponse;
import com.jdl.distribution.utils.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface VisitaService {

    @POST(Constants.WS_INICIAR_VISITA)
    Call<InitVisitResponse> doIniciarVisita(@Body String data);

    @POST(Constants.WS_RECUPERAR_VISITA)
    Call<InitVisitResponse> doRecuperarVisita(@Body String data);

}
