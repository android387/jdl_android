package com.jdl.distribution.mvp.model;

import com.developer.appsupport.annotation.stringnotnull.IStringNotNull;
import com.developer.appsupport.annotation.stringnotnull.StringNotNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class Moneda implements IStringNotNull<Moneda> {

    @StringNotNull
    @Expose
    @SerializedName("mon_id")
    private String id;
    @StringNotNull
    @Expose
    @SerializedName("mon_codigo")
    private String codigo;
    @StringNotNull
    @Expose
    @SerializedName("mon_descripcion")
    private String descripcion;
    @StringNotNull
    @Expose
    @SerializedName("mon_sigla")
    private String sigla;
    @StringNotNull
    @Expose
    @SerializedName("mon_tipo_cambio")
    private String tipoCambio;

    public String getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getSigla() {
        return sigla;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    @Override
    public String toString() {
        return "Moneda{" +
                "id='" + id + '\'' +
                ", codigo='" + codigo + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", sigla='" + sigla + '\'' +
                ", tipoCambio='" + tipoCambio + '\'' +
                '}';
    }

    @Override
    public Moneda getWithStringNotNull() {
        return getWithStringNotNull(this);
    }
}
