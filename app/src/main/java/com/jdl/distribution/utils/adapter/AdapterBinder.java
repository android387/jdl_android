package com.jdl.distribution.utils.adapter;

import androidx.annotation.Nullable;

public interface AdapterBinder<T> {

    void bindTo(@Nullable T t);

}
