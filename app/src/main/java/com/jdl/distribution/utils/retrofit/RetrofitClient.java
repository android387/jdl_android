package com.jdl.distribution.utils.retrofit;

import android.util.Log;

import com.jdl.distribution.BuildConfig;
import com.jdl.distribution.utils.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitClient {
    public static Retrofit retrofit;
    private static final String BASE_URL = Constants.URL_BASE;

    private RetrofitClient() {
    }

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            Log.d("BASE_URL",BASE_URL);
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getOkHttpClient())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getRetrofitInstance(String BASE_URL) {
        if (retrofit == null) {
            Log.d("BASE_URL",BASE_URL);

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getOkHttpClient())
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient getOkHttpClient() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG)
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        return new OkHttpClient.Builder()
                .addInterceptor(new HeaderInterceptor("application/json"))
                .callTimeout(60, TimeUnit.SECONDS)//default 10s
                .readTimeout(60, TimeUnit.SECONDS)//default 10s
                .writeTimeout(60, TimeUnit.SECONDS)//default 10s
                .connectTimeout(30, TimeUnit.SECONDS)//default 10s
                .addInterceptor(loggingInterceptor)//third, log at the end
                .build();
    }

}
