package com.jdl.distribution.utils.retrofit;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeaderInterceptor implements Interceptor {

    private String mAuth;
    private String json;

    public HeaderInterceptor(String basicUser, String basicPassword) {
        this.mAuth = Credentials.basic(basicUser, basicPassword);
    }
    public HeaderInterceptor(String json) {
        this.json = json;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request authenticatedRequest = request.newBuilder()
                .addHeader("Keep-Alive", "timeout=5, max=100")
                .addHeader("Connection","keep-alive")
                .addHeader("Cookie", "Set-Cookie")
                .addHeader("Content-Type",json).build();
//                .header("Authorization", mAuth).build();
        return chain.proceed(authenticatedRequest);
    }

}
