package com.jdl.distribution.utils.application;

import android.app.Application;

import com.developer.appsupport.utils.notification.SimpleNotification;
import com.jdl.distribution.R;
import com.jdl.distribution.data.db.AppDatabase;
import com.jdl.distribution.data.db.repository.DataRepository;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;

public class MyApplication extends Application {

    private AppExecutors appExecutors;

    @Override
    public void onCreate() {
        super.onCreate();

        appExecutors = new AppExecutors();
        InternetAvailabilityChecker.init(this);
        Logger.addLogAdapter(new AndroidLogAdapter());
        SimpleNotification.getInstance(this).builder()
                .setSmallIcon(R.mipmap.ic_logo)
                .setLargeIconRes(R.mipmap.ic_logo)
                .setVibrate(true)
                .build();

    }

//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//        MultiDex.install(this);
//    }

    public AppDatabase getDatabase() {
        return AppDatabase.getInstance(this, appExecutors);
    }

    public DataRepository getRepository() {
        return DataRepository.getInstance(getDatabase());
    }

    public AppExecutors getAppExecutors(){
        return appExecutors;
    }

}
