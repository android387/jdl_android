package com.developer.appsupport.ui.dialog.base;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.developer.appsupport.ui.dialog.MyDialogFragment;
import com.developer.appsupport.ui.fragment.IBaseFragment;
import com.developer.appsupport.ui.listener.MyLifecycleObserver;
import com.mijael.appsupport.R;

public class BaseDialogFragment extends MyDialogFragment implements IBaseFragment {

    private MyLifecycleObserver observer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        if (observer!=null)
            observer.onCreate();

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (observer!=null)
            observer.onResume();

        if (getDialog().getWindow()==null)
            return;

        if (getDialog().getWindow()!=null && getActivity()!=null)
            getDialog().getWindow().setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.back_transparent));


        WindowManager.LayoutParams layoutParams = getDialog().getWindow().getAttributes();
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes(layoutParams);

    }

    @Override
    public void onStart() {

        if (observer!=null)
            observer.onStart();

        super.onStart();
    }

    @Override
    public void onPause() {

        if (observer!=null)
            observer.onPause();

        super.onPause();
    }


    @Override
    public void onStop() {

        if (observer!=null)
            observer.onStop();

        super.onStop();
    }


    @Override
    public void onDestroy() {

        if (observer!=null)
            observer.onDestroy();

        super.onDestroy();
    }


    @Override
    public void setLifecycleObserver(MyLifecycleObserver observer) {
        this.observer = observer;
    }
}
