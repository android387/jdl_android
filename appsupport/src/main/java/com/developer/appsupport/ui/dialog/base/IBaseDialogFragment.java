package com.developer.appsupport.ui.dialog.base;

import com.developer.appsupport.ui.listener.MyLifecycleObserver;

public interface IBaseDialogFragment {
    void setLifecycleObserver(MyLifecycleObserver observer);
}
