package com.developer.appsupport.ui.fragment;

import com.developer.appsupport.ui.listener.MyLifecycleObserver;

public interface IBaseFragment {

    void setLifecycleObserver(MyLifecycleObserver observer);

}
