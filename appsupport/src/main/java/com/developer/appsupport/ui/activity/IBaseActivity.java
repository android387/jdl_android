package com.developer.appsupport.ui.activity;

import com.developer.appsupport.ui.listener.MyLifecycleObserver;

public interface IBaseActivity {

    void setLifecycleObserver(MyLifecycleObserver observer);

}
