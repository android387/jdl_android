package com.developer.appsupport.annotation.destroy;

import androidx.annotation.NonNull;

import com.developer.appsupport.ui.activity.BaseActivity;
import com.developer.appsupport.ui.activity.IBaseActivity;
import com.developer.appsupport.ui.fragment.BaseFragment;
import com.developer.appsupport.ui.fragment.IBaseFragment;

public class Subscriber {

    private IBaseActivity activity;
    private String token;
    private IBaseFragment fragment;

    public Subscriber(IBaseActivity activity, String token) {
        this.activity = activity;
        this.token = token;
    }


    public Subscriber(IBaseFragment fragment, String token) {
        this.token = token;
        this.fragment = fragment;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public IBaseActivity getActivity() {
        return activity;
    }

    public IBaseFragment getFragment() {
        return fragment;
    }

    @NonNull
    @Override
    public String toString() {
        if (fragment == null) {
            return "[acticity=" + activity.getClass().getSimpleName() + ", token=" + token + ", fragment=null]";
        } else {
            return "[acticity=null, token=" + token + ", fragment=" + fragment.getClass().getSimpleName() + "]";
        }
    }

}
