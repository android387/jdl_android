package com.developer.appsupport.annotation.onetime;

import android.view.View;

import com.developer.appsupport.ui.activity.IBaseActivity;
import com.developer.appsupport.ui.fragment.IBaseFragment;

public class Subscriber {

    private IBaseActivity activity;
    private IBaseFragment fragment;
    private View view;

    //activity

    public Subscriber(IBaseActivity activity, View view) {
        this.activity = activity;
        this.view = view;
    }


    //fragment

    public Subscriber(IBaseFragment fragment, View view) {
        this.fragment = fragment;
        this.view = view;
    }



    public IBaseActivity getActivity() {
        return activity;
    }

    public IBaseFragment getFragment() {
        return fragment;
    }

    public View getView() {
        return view;
    }

    @Override
    public String toString() {
        return "Subscriber{" +
                "activity=" + (activity==null?"null":activity.getClass().getSimpleName()) +
                ", fragment=" + (fragment==null?"null":fragment.getClass().getSimpleName()) +
                ", view=" +( (view==null)?"null":view.getId() ) +
                '}';
    }

}
