package com.developer.appsupport.annotation.destroy;

public enum Type {

    PROGRESS_DIALOG,
    ALERT_DIALOG,
    SWIPE_REFRESH,
    PROGRESS_BAR

}
