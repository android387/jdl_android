package com.developer.appsupport.mvp.presenter;

import com.developer.appsupport.mvp.model.Message;

public interface BasePresenter {

    void onStart();

    void onStop();

    void onMessageReceived(Message message);

}
